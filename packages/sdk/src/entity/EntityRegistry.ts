import { ethers, Signer } from 'ethers'
import { EntityRegistry as EntityRegistryJSON } from "@brreg/contracts";
import { GetProviderAndSigner, TX_OVERRIDES } from './../sdk'

// import { Signer } from 'ethers/abstract-signer';
import { Contract } from 'ethers/contract'
import * as R from 'remeda'
import { Web3Provider, TransactionResponse } from 'ethers/providers';
import { Proxy } from "./../proxy";

import { RegistryOfCapTables } from '../registryOfCapTables';
import { Stock, CapTableInfo, Transaction } from '../stock/Stock';


export interface CapTableTransactions {
  capTable: CapTableInfo
  transactions: Transaction[]
}

const FALLBACK_ENTITY = {
  address: "NO ENTITY FOUND",
  uuid: "NO ENTITY FOUND",
  type: "person",
  name: "NO ENTITY FOUND",
  country: "NO ENTITY FOUND",
  city: "NO ENTITY FOUND",
  postalcode: "NO ENTITY FOUND",
  streetAddress: "NO ENTITY FOUND",
}

export type EntityData = {
  address: string
  uuid: string
  type: string
  name: string
  country: string
  city: string
  postalcode: string
  streetAddress: string
}

export type TxData = {
  address: string
  uuid: string
  type: number
  name: string
  country: string
  city: string
  postalcode: string
  streetAddress: string
}

const entityDataStoredAsBytes = ['uuid', 'name', 'country', 'city', 'postalcode', 'streetAddress']

export class EntityRegistry {
  readonly contract: Contract
  readonly provider: Web3Provider
  readonly signer: Signer
  readonly proxy: Proxy


  constructor(web3Provider: Web3Provider, signer: Signer, proxy: Proxy, address: string) {
    this.provider = web3Provider;
    this.signer = signer;
    this.proxy = proxy
    this.contract = new ethers.Contract(address, EntityRegistryJSON.abi, this.signer)
  }

  static async init(externalSignerProvider: Signer | any, proxyAddress?: string): Promise<EntityRegistry> {
    const { provider, signer } = GetProviderAndSigner(externalSignerProvider)
    const proxy = (proxyAddress) ? new Proxy(provider, proxyAddress) : await Proxy.init(provider);
    const address = await proxy.entityRegistry()
    return new EntityRegistry(provider, signer, proxy, address)
  }

  async allTransactionsAllCapTables(uuid: string): Promise<CapTableTransactions[]> {
    const entity = await this.getEntityByUuid(uuid)
    const registryOfCapTables = new RegistryOfCapTables(this.provider, this.signer, this.proxy, await this.proxy.stockRegistry());
    const capTableList = await registryOfCapTables.list();
    return await Promise.all(capTableList.map(async (capTableInfo) => {
      const stock = new Stock(this.provider, this.signer, this.proxy, capTableInfo.address);
      const transactions = await stock.transactions({ address: entity.address });
      return {
        capTable: capTableInfo,
        transactions
      }
    }));
  }

  async getEntityByUuid(uuid: string): Promise<EntityData> {
    try {
      const uuidAsBytes32 = ethers.utils.formatBytes32String(uuid)
      const entity = await this.contract.getEntityByUuid(uuidAsBytes32)
      const addressForEntity = await this.contract.getAddressByUuid(uuidAsBytes32)
      const entityData = EntityRegistry._parseEntityData(entity)
      const entityDataWithAddress = { ...entityData, address: addressForEntity }
      return Promise.resolve(entityDataWithAddress)
    } catch (error) {
      console.log("Could NOT getEntityByUuid ", uuid);
      return Promise.resolve(FALLBACK_ENTITY)
    }
  }

  async getEntityByAddress(address: string): Promise<EntityData> {
    try {
      const entity = await this.contract.getEntityByAddress(address)
      const entityData = EntityRegistry._parseEntityData(entity)
      const entityDataWithAddress = { ...entityData, ...{ address } }
      return Promise.resolve(entityDataWithAddress)
    } catch (error) {
      return Promise.resolve(FALLBACK_ENTITY)
    }
  }

  async addEntity(data: EntityData): Promise<TransactionResponse> {
    const txData = EntityRegistry._encodeEntityData(data)
    return this.contract.addEntity(txData.address, txData.uuid, txData.type, txData.name, txData.country, txData.city, txData.postalcode, txData.streetAddress, TX_OVERRIDES)
  }

  async updateEntity(data: EntityData): Promise<TransactionResponse> {
    const txData = EntityRegistry._encodeEntityData(data)
    return this.contract.addEntity(txData.address, txData.uuid, txData.type, txData.name, txData.country, txData.city, txData.postalcode, txData.streetAddress, TX_OVERRIDES)
  }
  async generateAddress(): Promise<string> {
    return Promise.resolve(await (ethers.Wallet.createRandom()).getAddress())
  }

  private static _parseEntityData(txData: TxData): EntityData {
    let entitiyData = R.clone(txData)
    Object.keys(entitiyData).forEach(key => {
      // Type is a reserved word in Solidity so here we parse entityData to type.
      if (key === 'entityType') {
        if (entitiyData[key].toNumber() === 0) {
          entitiyData['type'] = 'person'
        } else if (entitiyData[key].toNumber() === 1) {
          entitiyData['type'] = 'organization'
        } else {
          throw 'Could not parse entity type'
        }
      }
      if (entityDataStoredAsBytes.indexOf(key) !== -1) {
        entitiyData[key] = ethers.utils.parseBytes32String(entitiyData[key])
      }
    })
    return entitiyData
  }

  private static _encodeEntityData(data: EntityData): TxData {
    let txData = R.clone(data)
    Object.keys(txData).forEach(key => {
      if (key === 'type') {
        if (txData[key].toLowerCase() === 'person') {
          txData[key] = 0
        } else if (txData[key].toLowerCase() === 'organization') {
          txData[key] = 1
        } else {
          throw 'Could not set entity type'
        }
      }
      if (entityDataStoredAsBytes.indexOf(key) !== -1) {
        txData[key] = ethers.utils.formatBytes32String(txData[key])
      }
    })
    return txData
  }


}
