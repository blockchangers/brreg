import { ethers, Signer } from 'ethers'
import { StockQue as StockQueJSON } from "@brreg/contracts";

// import { Signer } from 'ethers/abstract-signer';
import { Contract } from 'ethers/contract'
import { Web3Provider, TransactionResponse } from 'ethers/providers';
import { Proxy } from "../proxy";
import { GetProviderAndSigner, Stock, TX_OVERRIDES } from './../index'
import { EntityData, EntityRegistry } from '../entity/EntityRegistry';

export interface QueInfo {
  name: string
  totalSupply: number
  denomination: number
  director: EntityData
  address: string
  denominationPerShare: number
  isController: boolean
  isQued: Boolean
  queNumber: Number
  uuid: string
}

export class RegistryOfCapTablesQue {
  readonly contract: Contract
  readonly provider: Web3Provider
  readonly signer: Signer
  readonly proxy: Proxy



  constructor(web3Provider: Web3Provider, signer: Signer, proxy: Proxy, address: string) {
    this.provider = web3Provider;
    this.signer = signer;
    this.proxy = proxy
    this.contract = new ethers.Contract(address, JSON.parse(JSON.stringify(StockQueJSON.abi)), this.signer)
  }

  static async init(externalSignerProvider: Signer | any, proxyAddress?: string): Promise<RegistryOfCapTablesQue> {
    const { provider, signer } = GetProviderAndSigner(externalSignerProvider)
    const proxy = (proxyAddress) ? new Proxy(provider, proxyAddress) : await Proxy.init(provider);
    const address = await proxy.stockQue()
    return new RegistryOfCapTablesQue(provider, signer, proxy, address)
  }

  async addQue(address: string): Promise<TransactionResponse> {
    return this.contract.addQue(address, TX_OVERRIDES)
  }

  async process(address: string, approved: boolean, reason: string): Promise<TransactionResponse> {
    return this.contract.process(address, approved, reason, TX_OVERRIDES)
  }

  async isQued(address: string): Promise<boolean> {
    return Promise.resolve(await this.contract.isQued(address))
  }

  async queNumber(address: string): Promise<number> {
    return Promise.resolve((await this.contract.queNumber(address)).toString())
  }

  async que(): Promise<any[]> {
    let que: string[] = await this.contract.que()
    const entityRegistry = new EntityRegistry(this.provider, this.signer, this.proxy, await this.proxy.entityRegistry())


    const queInfoList = await Promise.all(que.reverse().slice(0, 10).map(async (address): Promise<any> => {
      try {
        const stock = new Stock(this.provider, this.signer, this.proxy, address);
        const isQued = await this.isQued(address)
        const queNumber = await this.queNumber(address)
        const name = await stock.name()
        const uuid = await stock.uuid()
        const totalSupply = await stock.totalSupply()
        let director = undefined
        const directorAddress = await stock.director()
        try {
          director = await entityRegistry.getEntityByAddress(directorAddress)
        } catch (error) {
          //
        }

        const data = {
          address: address,
          uuid: uuid,
          isQued: isQued,
          queNumber: queNumber,
          name: name ? name : "",
          totalSupply: totalSupply ? totalSupply : "",
          director: director ? director : "",
        }
        return Promise.resolve(data)
      } catch (error) {
        return Promise.resolve(undefined)
      }
    }))
    return queInfoList.filter(queItem => queItem !== undefined && queItem !== null)

  }
}
