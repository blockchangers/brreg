import { ethers, Signer } from 'ethers'
import { StockRegistry as StockRegistryJSON } from "@brreg/contracts";

// import { Signer } from 'ethers/abstract-signer';
import { Contract } from 'ethers/contract'
import { Web3Provider, TransactionResponse } from 'ethers/providers';
import { Proxy } from "../proxy";
import { Stock } from './../index'
import { GetProviderAndSigner, TX_OVERRIDES } from './../index'
import { CapTableInfo } from '../stock/Stock';

export class RegistryOfCapTables {
  readonly contract: Contract
  readonly provider: Web3Provider
  readonly signer: Signer
  readonly proxy: Proxy

  constructor(web3Provider: Web3Provider, signer: Signer, proxy: Proxy, address: string) {
    this.provider = web3Provider;
    this.signer = signer;
    this.proxy = proxy
    this.contract = new ethers.Contract(address, StockRegistryJSON.abi, this.signer)
  }

  static async init(externalSignerProvider: Signer | any, proxyAddress?: string): Promise<RegistryOfCapTables> {
    const { provider, signer } = GetProviderAndSigner(externalSignerProvider)
    const proxy = (proxyAddress) ? new Proxy(provider, proxyAddress) : await Proxy.init(provider);
    const address = await proxy.stockRegistry()
    return new RegistryOfCapTables(provider, signer, proxy, address)
  }

  async list(): Promise<CapTableInfo[]> {
    const stockList: string[] = await this.contract.stockList()

    return Promise.all(stockList.reverse().slice(0, 10).map(async (address): Promise<CapTableInfo> => {
      const stock = new Stock(this.provider, this.signer, this.proxy, address);
      const info = await stock.info()
      return Promise.resolve(info)
    }))
  }

  async add(address: string): Promise<TransactionResponse> {
    return this.contract.addStock(address, TX_OVERRIDES)
  }

  async disable(address: string): Promise<TransactionResponse> {
    return this.contract.removeStock(address, TX_OVERRIDES)
  }

}
