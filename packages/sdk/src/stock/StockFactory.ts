import { ethers, Signer } from 'ethers'
import { Web3Provider } from 'ethers/providers';
import { Stock } from './Stock'
import { Proxy } from "./../proxy";
import * as Contracts from '@brreg/contracts'
import { TX_OVERRIDES, GetProviderAndSigner } from "./../utils/index";

const DEFAULT_PARTITION = "0x4100000000000000000000000000000000000000000000000000000000000000"

export class StockFactory {
  readonly provider: Web3Provider
  readonly signer: Signer
  readonly proxy: Proxy
  private _qued: boolean

  constructor(web3Provider: Web3Provider, signer: Signer, proxy: Proxy) {
    this.provider = web3Provider;
    this.signer = signer;
    this.proxy = proxy
    this._qued = false
  }

  static async init(externalSignerProvider: Signer | any, proxyAddress?: string): Promise<StockFactory> {
    const { provider, signer } = GetProviderAndSigner(externalSignerProvider)
    const proxy = (proxyAddress) ? new Proxy(provider, proxyAddress) : await Proxy.init(provider);
    return new StockFactory(provider, signer, proxy)
  }

  async createNew(name: string, uuid: string, options: { partitions: string[], symbol: string } = { partitions: [DEFAULT_PARTITION], symbol: "" }): Promise<Stock> {
    const _symbol = (options.symbol === "") ? name.substr(0, 3).toUpperCase() : options.symbol.toUpperCase()

    const _uuidBytes32 = ethers.utils.formatBytes32String(uuid)
    const signerAddress = await this.signer.getAddress()
    let controllers: string[] = await this.proxy.getControllers()
    controllers.push(signerAddress)

    const factory = new ethers.ContractFactory(JSON.parse(JSON.stringify(Contracts.Stock.abi)), JSON.parse(JSON.stringify(Contracts.Stock.bytecode)), this.signer);

    let contract = await factory.deploy(name, _symbol, 1, controllers, signerAddress, options.partitions, _uuidBytes32, await this.proxy.erc820(), TX_OVERRIDES)
    await contract.deployed()

    let promises: Array<Promise<any>> = []
    promises.push(contract.deployed())
    promises.push(this.addStockQue(contract.address))
    await Promise.all(promises)
    return new Stock(this.provider, this.signer, this.proxy, contract.address);
  }


  async addStockQue(address: string): Promise<boolean> {
    const stockQueContract = new ethers.Contract(await this.proxy.stockQue(), Contracts.StockQue.abi, this.signer)
    await (await stockQueContract.addQue(address, TX_OVERRIDES)).wait()
    const qued = await stockQueContract.isQued(address)
    if (qued) {
      this._qued = true
    } else {
      return Promise.reject('Could not verify that the Stock was added to StockQueJSON')
    }
    return Promise.resolve(this._qued)
  }

  // async addStockQue(address: string): Promise<boolean> {
  //   const stockQueContract = new ethers.Contract(await this.proxy.stockQue(), Contracts.StockQue.abi, this.signer)
  //   const tx = await stockQueContract.addQue(address, TX_OVERRIDES)
  //   await tx.wait()
  //   return Promise.resolve(true)
  // }

  async qued(): Promise<boolean> {
    return Promise.resolve(this._qued)
  }
}
