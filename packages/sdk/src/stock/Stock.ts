import { ethers } from 'ethers'
import { Stock as StockJSON } from "@brreg/contracts";
import { Signer } from 'ethers/abstract-signer';
import { Contract } from 'ethers/contract'
import { GetProviderAndSigner, EntityRegistry, TX_OVERRIDES } from "./../index";
import { Web3Provider, TransactionResponse, Log } from 'ethers/providers';
import { Proxy } from "../proxy";
import { EntityData } from "../entity/EntityRegistry";
import { LogDescription } from 'ethers/utils';
import { flatten } from 'remeda'

const VALID_DATA_CERTIFICATE = "0x1000000000000000000000000000000000000000000000000000000000000000"
const EVENT_ABI: { [s: string]: string } = {
  'transfer': ethers.utils.id(`TransferByPartition(bytes32,address,address,address,uint256,bytes,bytes)`), // event TransferByPartition(bytes32 indexed fromPartition, address operator, address indexed from, address indexed to, uint256 value, bytes data, bytes operatorData)
  'issue': ethers.utils.id('IssuedByPartition(bytes32,address,address,uint256,bytes,bytes)'), // event IssuedByPartition(bytes32 indexed partition, address indexed operator, address indexed to, uint256 value, bytes data, bytes operatorData)
  'redeem': ethers.utils.id('RedeemedByPartition(bytes32,address,address,uint256,bytes,bytes)') // event RedeemedByPartition(bytes32 indexed partition, address indexed operator, address indexed from, uint256 value, bytes data, bytes operatorData)
}

export interface Shareholder extends EntityData {
  partition: string,
  balance: number,
}

export type Transaction = {
  partition: string
  amount: number
  data: string,
  operatorData: string
  to?: string
  toEntity?: EntityData
  from?: string
  fromEntity?: EntityData
  blockNumber: number
  transactionHash: string
  date: Date,
  transactionType: string
}

export interface LogAndMeta extends LogDescription {
  meta: Log
}

export interface CapTableInfo {
  name: string
  uuid: string
  totalSupply: number
  denomination: number
  directorAddress: string
  director: EntityData
  address: string
  denominationPerShare: number
  isController: boolean,
}


export class Stock {
  readonly contract: Contract
  readonly provider: Web3Provider
  readonly signer: Signer
  readonly proxy: Proxy

  constructor(web3Provider: Web3Provider, signer: Signer, proxy: Proxy, address: string) {
    this.provider = web3Provider;
    this.signer = signer;
    this.proxy = proxy
    this.contract = new ethers.Contract(address, StockJSON.abi, signer)
  }

  static async init(externalSignerProvider: Signer | any, address: string, proxyAddress?: string): Promise<Stock> {
    const { provider, signer } = GetProviderAndSigner(externalSignerProvider)
    const proxy = (proxyAddress) ? new Proxy(provider, proxyAddress) : await Proxy.init(provider);
    return new Stock(provider, signer, proxy, address)
  }

  // async transactions(): Promise<any> {
  //   const entityRegistry = await new EntityRegistry(this.provider, this.signer, this.proxy, await this.proxy.entityRegistry())
  //   const allTranscations = await getAllTransactions(this.provider, this.contract, entityRegistry);
  //   return allTranscations;
  // }

  async transactions(filter?: { address?: string }): Promise<Transaction[]> {
    const logs = await this.getLogs()
    const transactions = await this.getTransactionsFromLogs(logs, filter)
    return transactions
  }

  async shareholders(): Promise<Shareholder[]> {
    const logs = await this.getLogs()
    const shareholders = await this.getShareholdersFromLogs(logs)
    return shareholders
  }

  async symbol(): Promise<string> {
    return await this.contract.symbol()
  }

  async name(): Promise<string> {
    return await this.contract.name()
  }

  async uuid(): Promise<string> {
    return ethers.utils.parseBytes32String(await this.contract.uuid())
  }

  async granularity(): Promise<number> {
    return (await this.contract.granularity()).toNumber()
  }

  async totalSupply(): Promise<number> {
    return (await this.contract.totalSupply()).toNumber()
  }

  async denomination(): Promise<number> {
    return (await this.contract.denomination()).toNumber()
  }
  async isController(address: string): Promise<boolean> {
    return await this.contract.isController(address)
  }

  async setDenomination(newDenomination: number): Promise<TransactionResponse> {
    const denomination6decimals = newDenomination
    return await this.contract.setDenomination(denomination6decimals, TX_OVERRIDES)
  }

  async denominationPerShare(): Promise<number> {
    const denomination = await this.denomination()
    const totalSupply = await this.totalSupply()
    return (denomination / totalSupply > 0) ? denomination / totalSupply : 0
  }

  async director(): Promise<string> {
    return await this.contract.getDirector()
  }

  async transferDirector(address: string): Promise<TransactionResponse> {
    return await this.contract.transferDirector(address);
  }

  async info(): Promise<CapTableInfo> {
    const entityRegistry = new EntityRegistry(this.provider, this.signer, this.proxy, await this.proxy.entityRegistry())
    const name = await this.name()
    const uuid = await this.uuid()
    const totalSupply = await this.totalSupply()
    const directorAddress = await this.director()
    const director = await entityRegistry.getEntityByAddress(directorAddress)
    const address = await this.getAddress()
    const denomination = await this.denomination()
    const denominationPerShare = await this.denominationPerShare()
    const isController = await this.isController(await this.signer.getAddress())
    return {
      name,
      uuid,
      totalSupply,
      denomination,
      denominationPerShare,
      directorAddress,
      director,
      address,
      isController
    }
  }

  async issue(toUuid: string, numberOfSharesToTransfer: number, options?: { data?: string, partition?: string }): Promise<TransactionResponse> {
    const entityRegistry = await new EntityRegistry(this.provider, this.signer, this.proxy, await this.proxy.entityRegistry())
    const to = await entityRegistry.getEntityByUuid(toUuid)
    if (!options) {
      options = {}
    }
    if (!options.partition) {
      options.partition = (await this.getDefaultPartitions(true))[0]
    } else {
      options.partition = ethers.utils.formatBytes32String(options.partition)
    }
    if (!options.data) {
      options.data = VALID_DATA_CERTIFICATE
    }
    return await this.contract.issueByPartition(options.partition, to.address, numberOfSharesToTransfer, options.data, TX_OVERRIDES)
  }


  async transfer(toUuid: string, numberOfSharesToTransfer: number, options?: { partition?: string, data?: string }): Promise<TransactionResponse> {
    const entityRegistry = await new EntityRegistry(this.provider, this.signer, this.proxy, await this.proxy.entityRegistry())
    const to = await entityRegistry.getEntityByUuid(toUuid)
    if (!options) {
      options = {}
    }
    if (!options.partition) {
      options.partition = (await this.getDefaultPartitions(true))[0]
    } else {
      options.partition = ethers.utils.formatBytes32String(options.partition)
    }
    if (!options.data) {
      options.data = VALID_DATA_CERTIFICATE
    }
    return this.contract.transferByPartition(options.partition, to.address, numberOfSharesToTransfer, options.data, TX_OVERRIDES)
  }

  async operatorTransfer(fromUuid: string, toUuid: string, numberOfSharesToTransfer: number, options?: { partition?: string, data?: string, operatorData?: string }): Promise<TransactionResponse> {
    const entityRegistry = await new EntityRegistry(this.provider, this.signer, this.proxy, await this.proxy.entityRegistry())
    const from = await entityRegistry.getEntityByUuid(fromUuid)
    const to = await entityRegistry.getEntityByUuid(toUuid)
    if (!options) {
      options = {}
    }
    if (!options.partition) {
      options.partition = (await this.getDefaultPartitions(true))[0]
    } else {
      options.partition = ethers.utils.formatBytes32String(options.partition)
    }
    if (!options.data) {
      options.data = VALID_DATA_CERTIFICATE
    }
    if (!options.operatorData) {
      options.operatorData = await this.signer.getAddress();
    }
    return await this.contract.operatorTransferByPartition(options.partition, from.address, to.address, numberOfSharesToTransfer, options.data, options.operatorData, TX_OVERRIDES);
  }

  async redeem(numberOfSharesToRedeem: number, options?: { partition?: string, data?: string }): Promise<TransactionResponse> {
    if (!options) {
      options = {}
    }
    if (!options.partition) {
      options.partition = (await this.getDefaultPartitions(true))[0]
    }
    if (!options.data) {
      options.data = VALID_DATA_CERTIFICATE
    }
    return await this.contract.redeemByPartition(options.partition, numberOfSharesToRedeem, options.data, TX_OVERRIDES)
  }

  async operatorRedeem(fromUuid: string, numberOfSharesToRedeem: number, options?: { partition?: string, data?: string, operatorData?: string }): Promise<TransactionResponse> {
    const entityRegistry = await new EntityRegistry(this.provider, this.signer, this.proxy, await this.proxy.entityRegistry())
    const from = await entityRegistry.getEntityByUuid(fromUuid)
    if (!options) {
      options = {}
    }
    if (!options.partition) {
      options.partition = (await this.getDefaultPartitions(true))[0]
    }
    if (!options.data) {
      options.data = VALID_DATA_CERTIFICATE
    }
    if (!options.operatorData) {
      options.operatorData = await this.signer.getAddress();
    }
    return await this.contract.operatorRedeemByPartition(options.partition, from.address, numberOfSharesToRedeem, options.data, options.operatorData, TX_OVERRIDES);
  }



  async getDefaultPartitions(asBytes = false): Promise<string[]> {
    const bytes32Array = await this.contract.getTokenDefaultPartitions()
    if (asBytes) {
      return bytes32Array
    } else {
      return bytes32Array.map((bytes32: string): string => {
        return ethers.utils.parseBytes32String(bytes32)
      })
    }
  }

  async balanceOf(address: string): Promise<number> {
    return (await this.contract.balanceOf(address)).toNumber()
  }

  async balanceOfByPartition(partition: string, address: string): Promise<number> {
    return (await this.contract.balanceOfByPartition(ethers.utils.formatBytes32String(partition), address)).toNumber()
  }

  async partitionsOf(address: string, asBytes = false): Promise<string[]> {
    const bytes32Array = await this.contract.partitionsOf(address)
    if (asBytes) {
      return bytes32Array
    } else {
      return bytes32Array.map((bytes32: string): string => {
        return ethers.utils.parseBytes32String(bytes32)
      })
    }
  }

  async getTotalSupplyByPartition(partition: string): Promise<number> {
    return await this.contract.getTotalSupplyByPartition(partition)
  }

  async getAddress(): Promise<string> {
    return Promise.resolve(this.contract.address)
  }

  async address(): Promise<string> {
    return await this.getAddress()
  }

  /* PRIVATE FUNCTIONS START */

  private async getShareholdersFromLogs(logs: LogDescription[]): Promise<Shareholder[]> {
    // Get all possible addresses
    const shareholderAddresses = logs.filter(log => {
      if (log.values) {
        if (log.values.to) {
          return true
        }
      }
      return false
    }).map(log => {
      return log.values.to
    })
    const entityRegistry = await new EntityRegistry(this.provider, this.signer, this.proxy, await this.proxy.entityRegistry())
    //Map over each shareholder address to get entity and balances by partition
    const shareholderPromises = shareholderAddresses.map(async (address): Promise<any> => {
      let entity = {}
      try {
        entity = await entityRegistry.getEntityByAddress(address)
      } catch (error) {
        console.log("No entity");
      }
      const partitions = await this.partitionsOf(address)
      const partitionPromises = partitions.map(async (partition): Promise<{ partition: string, balance: number }> => {
        const balanceOfByPartition = await this.balanceOfByPartition(partition, address)
        return {
          partition,
          balance: balanceOfByPartition
        }
      })
      const balances = await Promise.all(partitionPromises)
      return Promise.resolve({
        ...entity,
        address,
        balances
      })
    })
    const shareholders = await Promise.all(shareholderPromises)
    // Expand so that there is one shareholer per partions balance
    let shareholdersByPartition: Shareholder[] = []
    shareholders.forEach(shareholder => {
      const balances = shareholder.balances
      balances.forEach((balance: { partition: string; balance: number; }) => {
        shareholdersByPartition.push({ ...shareholder, partition: balance.partition, balance: balance.balance })
      })
    })
    return Promise.resolve(shareholdersByPartition)
  }


  private async getTransactionsFromLogs(logs: LogAndMeta[], filter?: { address?: string }): Promise<Transaction[]> {
    const entityRegistry = await new EntityRegistry(this.provider, this.signer, this.proxy, await this.proxy.entityRegistry())
    const promises = logs.map(async (log): Promise<any> => {
      let shareholderTx: Transaction = {
        partition: "",
        amount: NaN,
        data: "",
        operatorData: "",
        to: "",
        toEntity: undefined,
        from: "",
        fromEntity: undefined,
        blockNumber: NaN,
        transactionHash: "",
        date: new Date(),
        transactionType: ""
      }
      if (log.values) {
        if (log.values.to) {
          shareholderTx.to = log.values.to
          try {
            shareholderTx.toEntity = await entityRegistry.getEntityByAddress(log.values.to)
          } catch (error) {
            console.log("Transaction TO did not have any entity");
          }
        }
        if (log.values.from) {
          shareholderTx.from = log.values.from
          try {
            shareholderTx.fromEntity = await entityRegistry.getEntityByAddress(log.values.from)
          } catch (error) {
            console.log("Transaction FROM did not have any entity");
          }
        }
        if (log.values.partition) {
          shareholderTx.partition = ethers.utils.parseBytes32String(log.values.partition)
        }
        if (log.values.value /* && log.values.value instanceof BigNumber */) {
          shareholderTx.amount = log.values.value.toNumber()
        }
        if (log.values.data) {
          shareholderTx.data = log.values.data
        }
        if (log.values.operatorData) {
          shareholderTx.operatorData = log.values.operatorData
        }
        if (log.name) {
          shareholderTx.transactionType = log.name
        }
        // Meta
        if (log.meta.transactionHash) {
          shareholderTx.transactionHash = log.meta.transactionHash
        }
        if (log.meta.blockNumber) {
          shareholderTx.blockNumber = log.meta.blockNumber
        }
        if (log.meta.blockNumber) {
          const block = await this.provider.getBlock(log.meta.blockNumber)
          if (block) {
            shareholderTx.date = new Date(block.timestamp * 1000)
          }
        }
      }
      return Promise.resolve(shareholderTx)
    })
    const unfilteredTransactions = await Promise.all(promises)
    return unfilteredTransactions.filter(tx => {
      if (filter) {
        if (filter.address) {
          if (tx.to === filter.address || tx.from === filter.address) {
            return true
          } else {
            return false
          }
        }
      }
      return true
    })
  }


  private async getLogs(types: string[] = ['transfer', 'issue', 'redeem']): Promise<LogAndMeta[]> {
    const logPromises = types.map(async (type): Promise<LogAndMeta[]> => {
      const filter = {
        address: this.contract.address,
        fromBlock: 0,
        toBlock: 'latest',
        topics: [EVENT_ABI[type]]
      }
      const unparsedLogs = await this.provider.getLogs(filter)
      return unparsedLogs.map(log => {
        return {
          ...this.contract.interface.parseLog(log),
          meta: { ...log }
        }
      })
    })
    const logsArrayArray = await Promise.all(logPromises)
    const logsArray = flatten(logsArrayArray)
    return Promise.resolve(logsArray)
  }

  /* PRIVATE FUNCTIONS END */

  /* TO BE REFACTORED OUT */
  async operatorTransferByPartition(partition: string, fromAddress: string, toAddress: string, numberOfShareToTransfer: number, data: string = VALID_DATA_CERTIFICATE, operatorData?: string): Promise<string> {
    const operator = operatorData ? operatorData : await this.signer.getAddress();
    return await this.contract.operatorTransferByPartition(ethers.utils.formatBytes32String(partition), fromAddress, toAddress, numberOfShareToTransfer, data, operator, TX_OVERRIDES);
  }

  async issueByPartition(partition: string, tokenHolder: string, numberOfSharesToIssue: number, data: string) {
    return await this.contract.issueByPartition(partition, tokenHolder, numberOfSharesToIssue, data, TX_OVERRIDES)
  }

  async transferByPartition(partition: string, toAddress: string, numberOfShareToTransfer: number, data: string, signer: Signer): Promise<string> {
    return await this.contract.connect(signer).transferByPartition(partition, toAddress, numberOfShareToTransfer, data, TX_OVERRIDES)
  }

  async redeemByPartition(partition: string, numberOfSharesToRedeem: number, data: string, signer: Signer) {
    return await this.contract.connect(signer).redeemByPartition(partition, numberOfSharesToRedeem, data, TX_OVERRIDES)
  }

  async operatorRedeemByPartition(partition: string, tokenHolder: string, numberOfSharesToRedeem: number, data: string, operatorData: string, signer: Signer) {
    return await this.contract.connect(signer).redeemByPartition(partition, tokenHolder, numberOfSharesToRedeem, data, operatorData, TX_OVERRIDES)
  }
  /* TO BE REFACTORED OUT END */



}
