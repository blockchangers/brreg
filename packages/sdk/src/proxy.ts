import { ethers, Contract } from 'ethers'
import { Provider } from 'ethers/providers';
import { CapTableSystemProxy as CapTableSystemProxyJSON } from '@brreg/contracts'

export class Proxy {
    private readonly contract: Contract

    constructor(provider: Provider, contractAddress = '0xEFacf9D5B79767C7BB21EC2f2C582c2fF4A40240') {
        this.contract = new ethers.Contract(contractAddress, CapTableSystemProxyJSON.abi, provider)
    }

    static async init(provider: Provider | any): Promise<Proxy> {
        const network = await provider.getNetwork()
        //console.log("Network", network);
        if (network && network.chainId && network.chainId !== 0) {
            const proxyAddressForNetworkId: { [n: number]: string } = {
                5: '0xEFacf9D5B79767C7BB21EC2f2C582c2fF4A40240', // Goerli
                2018: '0x9CD49EbB8f39BD510eab6b8f6038c2010CC6126c',
                5777: '0x8F9a2945298E83463F4c8E3014f7d236f02Fea17',
                5799: '0x15Bccf54919Fe21904f97664BA25c3f557D68E25',
                53387025: '0x5B04fb49dda2576062ff93C069748242A59BF60F',
                99999999: '0xE89467c417628D4dd32DD4207964CB2Eb5d9E88a'
            }
            // console.log("Useing proxy from ", proxyAddressForNetworkId[network.chainId]);
            return new Proxy(provider, proxyAddressForNetworkId[network.chainId])
        }
        throw "Could not determine network provider and therefor could not determine proxy address in Proxy.ts"
    }

    async erc820(): Promise<string> {
        const contractNameBytes32 = ethers.utils.formatBytes32String('erc820')
        return await this.contract.getContract(contractNameBytes32)
    }

    async stockRegistry(): Promise<string> {
        const contractNameBytes32 = ethers.utils.formatBytes32String('stockRegistry')
        return await this.contract.getContract(contractNameBytes32)
    }

    async stockQue(): Promise<string> {
        const contractNameBytes32 = ethers.utils.formatBytes32String('stockQue')
        return await this.contract.getContract(contractNameBytes32)
    }

    async entityRegistry(): Promise<string> {
        const contractNameBytes32 = ethers.utils.formatBytes32String('entityRegistry')
        return await this.contract.getContract(contractNameBytes32)
    }

    async getAddressForContract(contractName: string): Promise<string> {
        const contractNameBytes32 = ethers.utils.formatBytes32String(contractName)
        return await this.contract.getContract(contractNameBytes32)
    }

    async getControllers(): Promise<string[]> {
        return await this.contract.listControllers()
    }
}