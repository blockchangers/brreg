import { Proxy } from "./../proxy";
import { ethers, Signer } from 'ethers'
import { Web3Provider } from 'ethers/providers';
import { GetProviderAndSigner, TX_OVERRIDES } from './../index'
import { Contract } from 'ethers/contract'

import { ERC820Registry as ERC820RegistryJSON } from "@brreg/contracts";

export class ERC820Registry {
  readonly contract: Contract
  readonly provider: Web3Provider
  readonly signer: Signer
  readonly proxy: Proxy

  constructor(web3Provider: Web3Provider, signer: Signer, proxy: Proxy, address: string) {
    this.provider = web3Provider;
    this.signer = signer;
    this.proxy = proxy
    this.contract = new ethers.Contract(address, ERC820RegistryJSON.abi, this.signer)
  }

  static async init(externalSignerProvider: Signer | any, proxyAddress?: string): Promise<ERC820Registry> {
    const { provider, signer } = GetProviderAndSigner(externalSignerProvider)
    const proxy = (proxyAddress) ? new Proxy(provider, proxyAddress) : await Proxy.init(provider);
    const address = await proxy.erc820()
    return new ERC820Registry(provider, signer, proxy, address)
  }

  async setManager(address: string, newManagerAddress: string) {
    return await this.contract.setManager(address, newManagerAddress, TX_OVERRIDES);
  }

  async getManager(address: string): Promise<string> {
    return await this.contract.getManager(address);
  }

}
