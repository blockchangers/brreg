import { Provider, Web3Provider } from "ethers/providers";
import { Signer, ethers } from "ethers";

export const GetSigner = (provider: Provider | any) => {
    try {
        if (provider instanceof Signer) {
            return provider
        }
        return provider.getSigner();
    } catch (error) {
        console.log("Ethers gave eror when trying this.provider.getSigner() ", error)
        try {
            return provider.getSigner(0);
        } catch (error) {
            console.log("Ethers gave eror when trying this.provider.getSigner(0) ", error)
            throw "Could not get signer from provider object"
        }
    }
}

export const GetProviderAndSigner = (externalProvider: Web3Provider | any): { provider: Web3Provider | any, signer: Signer } => {
    let web3Provider
    let signer
    if (externalProvider.provider instanceof Provider) {
        // Probably sending in a signer
        web3Provider = externalProvider.provider
        signer = GetSigner(externalProvider);
    } else {
        // Probably sending in from metamask
        web3Provider = new ethers.providers.Web3Provider(externalProvider)
        signer = GetSigner(web3Provider);
    }
    return {
        provider: web3Provider,
        signer: signer
    }
}

export const TX_OVERRIDES = {
    // The maximum units of gas for the transaction to use
    gasLimit: 50000000,
    // The price (in wei) per unit of gas
    gasPrice: ethers.utils.parseUnits('0.0', 'gwei'),
    // chainId: 53387025
    // chainId: 2018 // todo: cHEKCK IF THIS IS NEEDED
};
