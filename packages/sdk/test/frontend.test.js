'use strict'
const expect = require('chai').expect;
const brregSdk = require('../lib/index')
const ethers = require('ethers')
const randomData = require('./mock/data')
const { getProvider } = require('./utils/provider');
const Deployer = require('@brreg/contracts').Deployer;
const replace = require('replace-in-file');

const provider = getProvider(process.env.NETWORK || 'norchain')

describe('Frotend test', async function () {
    this.timeout(80000);
    it("Setup accounts", async () => {
        this.CONTROLLER_SIGNER = ethers.Wallet.createRandom().connect(provider)
        this.DIRECTOR_SIGNER = ethers.Wallet.createRandom().connect(provider)
        this.USER_SIGNER = ethers.Wallet.createRandom().connect(provider)
        //this.USER_SIGNER = ethers.Wallet.fromMnemonic('forget silent snap unlock federal elegant stool celery fetch salt draw cave').connect(provider)
        this.CONTROLLER = await this.CONTROLLER_SIGNER.getAddress()
        this.DIRECTOR = await this.DIRECTOR_SIGNER.getAddress()
        this.USER = await this.USER_SIGNER.getAddress()
    })

    it('Setup deployer', () => {
        this.deployer = new Deployer(this.CONTROLLER_SIGNER)
    });

    it('Deploy erc820', async () => {
        this.erc820 = await this.deployer.erc820()
        expect(this.erc820.address).length.to.be.greaterThan(5)
    });

    it('Deploy stockQue', async () => {
        this.stockQue = await this.deployer.stockQue([this.CONTROLLER])
        expect(this.stockQue.address).length.to.be.greaterThan(5)
    });

    it('Deploy stockRegistry', async () => {
        this.stockRegistry = await this.deployer.stockRegistry([this.CONTROLLER])
        expect(this.stockRegistry.address).length.to.be.greaterThan(5)
    });

    it('Deploy entityRegistry', async () => {
        this.entityRegistry = await this.deployer.entityRegistry([this.CONTROLLER])
        expect(this.entityRegistry.address).length.to.be.greaterThan(5)
    });

    it('Deploy capTableSystemProxy', async () => {

        console.log(1)
        this.capTableSystemProxy = await this.deployer.capTableSystemProxy([this.CONTROLLER])
        expect(this.capTableSystemProxy.address).length.to.be.greaterThan(5)
        console.log(2)
        let tx1 = await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('erc820'), this.erc820.address, brregSdk.TX_OVERRIDES)
        await tx1.wait()
        console.log(3)
        await setTimeout(() => {
            return Promise.resolve()
        }, 9000)
        console.log(4)
        let tx2 = await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('stockQue'), this.stockQue.address, brregSdk.TX_OVERRIDES)
        await tx2.wait()
        await setTimeout(() => {
            return Promise.resolve()
        }, 9000)
        let tx3 = await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('stockRegistry'), this.stockRegistry.address, brregSdk.TX_OVERRIDES)
        await tx3.wait()
        await setTimeout(() => {
            return Promise.resolve()
        }, 9000)
        let tx4 = await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('entityRegistry'), this.entityRegistry.address, brregSdk.TX_OVERRIDES)
        await tx4.wait()
        await setTimeout(() => {
            return Promise.resolve()
        }, 9000)
        // expect(await this.capTableSystemProxy.getContract(ethers.utils.formatBytes32String('erc820'))).to.be.equal(this.erc820.address)
        this.PROXY_ADDRESS = this.capTableSystemProxy.address
    });


    describe('#TestDeploy - Deploy and generate test data', async () => {



        it('Check that proxy is working', async () => {
            expect(await this.capTableSystemProxy.getContract(ethers.utils.formatBytes32String('erc820'))).to.be.equal(this.erc820.address)
        });

        it('Setup entities for accounts', async () => {
            this.entityRegistry = await brregSdk.EntityRegistry.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            const person1 = randomData.shareholder()
            const person2 = randomData.shareholder()
            const person3 = randomData.shareholder()
            await (await this.entityRegistry.addEntity({ ...person1, address: this.CONTROLLER })).wait()
            await (await this.entityRegistry.addEntity({ ...person2, address: this.DIRECTOR })).wait()
            await (await this.entityRegistry.addEntity({ ...person3, address: this.USER })).wait()
            this.CONTROLLER_ENTITY = await this.entityRegistry.getEntityByAddress(this.CONTROLLER);
            this.DIRECTOR_ENTITY = await this.entityRegistry.getEntityByAddress(this.DIRECTOR);
        });

        it('Create company emptyInc', async () => {
            this.entityRegistry = await brregSdk.EntityRegistry.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            const person1 = randomData.shareholder()
            const person1address = randomData.address()
            await (await this.entityRegistry.addEntity({ ...person1, address: person1address })).wait()

            const testCompany = randomData.company()
            console.log("Before")
            const stockFactory = await brregSdk.StockFactory.init(this.DIRECTOR_SIGNER, this.PROXY_ADDRESS)

            const emptyInc = await stockFactory.createNew("Empty inc.", testCompany.uuid)
            console.log("after")
            const stockQue = await brregSdk.RegistryOfCapTablesQue.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await (await stockQue.process(await emptyInc.getAddress(), true, "Test that process is working")).wait()

            const registry = await brregSdk.RegistryOfCapTables.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await (await registry.add(await emptyInc.getAddress())).wait()
        });

        it('Create company TransActions AS', async () => {
            this.entityRegistry = await brregSdk.EntityRegistry.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            const OWNER_SIGNER = ethers.Wallet.createRandom().connect(provider)
            let OWNER_DATA = randomData.shareholder()
            OWNER_DATA.name = "Trans Lurum"
            const OWNER = await OWNER_SIGNER.getAddress()
            await this.entityRegistry.addEntity({ ...OWNER_DATA, address: OWNER });

            const testCompany = randomData.company()
            const stockFactory = await brregSdk.StockFactory.init(OWNER_SIGNER, this.PROXY_ADDRESS)
            const transActionsAS = await stockFactory.createNew("Trans Actions AS", testCompany.uuid)

            const stockQue = await brregSdk.RegistryOfCapTablesQue.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await stockQue.process(transActionsAS.getAddress(), true, "Approved from Que")

            const registry = await brregSdk.RegistryOfCapTables.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await registry.add(await transActionsAS.getAddress())

            await transActionsAS.issue(OWNER_DATA.uuid, 500);
            await transActionsAS.issue(this.CONTROLLER_ENTITY.uuid, 200);
            await transActionsAS.issue(this.DIRECTOR_ENTITY.uuid, 100);

            // Director
            await transActionsAS.transfer(this.DIRECTOR_ENTITY.uuid, 20);
            await transActionsAS.transfer(this.CONTROLLER_ENTITY.uuid, 20);

        });

        it('Create SemiControllers', async () => {
            const testCompany = randomData.company()
            const stockFactory = await brregSdk.StockFactory.init(this.DIRECTOR_SIGNER, this.PROXY_ADDRESS)
            this.SEMI_CONTROLLERS_COMPANY = await stockFactory.createNew("SemiControllers AS", testCompany.uuid)

            const stockQue = await brregSdk.RegistryOfCapTablesQue.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await (await stockQue.process(this.SEMI_CONTROLLERS_COMPANY.getAddress(), true, "Company CapTable approved by test.")).wait()

            const registry = await brregSdk.RegistryOfCapTables.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await (await registry.add(await this.SEMI_CONTROLLERS_COMPANY.getAddress())).wait()
        });

        it('create Transactions', async () => {
            this.entityRegistry = await brregSdk.EntityRegistry.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            const person1 = randomData.shareholder()
            const person2 = randomData.shareholder()
            await (await this.entityRegistry.addEntity({ ...person1 })).wait()
            await (await this.entityRegistry.addEntity({ ...person2 })).wait()

            await (await this.SEMI_CONTROLLERS_COMPANY.issue(person1.uuid, 1000)).wait()
            await (await this.SEMI_CONTROLLERS_COMPANY.issue(person2.uuid, 800)).wait()
            await (await this.SEMI_CONTROLLERS_COMPANY.issue(this.DIRECTOR_ENTITY.uuid, 600)).wait()
            await (await this.SEMI_CONTROLLERS_COMPANY.issue(this.CONTROLLER_ENTITY.uuid, 400)).wait()
            await (await this.SEMI_CONTROLLERS_COMPANY.issue(person2.uuid, 400, { partition: "B" })).wait()
            await (await this.SEMI_CONTROLLERS_COMPANY.issue(person1.uuid, 400, { partition: "B" })).wait()
            await (await this.SEMI_CONTROLLERS_COMPANY.issue(this.DIRECTOR_ENTITY.uuid, 400, { partition: "B" })).wait()
            await (await this.SEMI_CONTROLLERS_COMPANY.issue(this.CONTROLLER_ENTITY.uuid, 400, { partition: "B" })).wait()
            await (await this.SEMI_CONTROLLERS_COMPANY.operatorTransfer(person1.uuid, person2.uuid, 350)).wait()
            await (await this.SEMI_CONTROLLERS_COMPANY.redeem(50)).wait()
            await (await this.SEMI_CONTROLLERS_COMPANY.transfer(this.CONTROLLER_ENTITY.uuid, 250)).wait()
        });

        it('Create company', async () => {
            const testCompany = randomData.company()
            const stockFactory = await brregSdk.StockFactory.init(this.USER_SIGNER, this.PROXY_ADDRESS)
            this.USEABLE_COMPANY = await stockFactory.createNew("UseableCompany AS", testCompany.uuid)

            const stockQue = await brregSdk.RegistryOfCapTablesQue.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await (await stockQue.process(await this.USEABLE_COMPANY.getAddress(), true, "Company CapTable approved by test.")).wait()

            const registry = await brregSdk.RegistryOfCapTables.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await (await registry.add(await this.USEABLE_COMPANY.getAddress())).wait()
        });

        it('Set Denomination', async () => {
            await (await this.USEABLE_COMPANY.setDenomination(5000)).wait()
            expect(await this.USEABLE_COMPANY.denomination()).to.be.equal(5000)
        });

        it('Get correct denomination per share', async () => {
            await (await this.USEABLE_COMPANY.issue(this.CONTROLLER_ENTITY.uuid, 2500)).wait()
            expect(await this.USEABLE_COMPANY.denominationPerShare()).to.be.equal(2)
            await (await this.USEABLE_COMPANY.issue(this.CONTROLLER_ENTITY.uuid, 7500)).wait()
            expect(await this.USEABLE_COMPANY.denominationPerShare()).to.be.equal(0.5)
        });

        it('Log out', async () => {
            if (true) {
                console.log("CONTROLLER_SIGNER: ", this.CONTROLLER, "\n", this.CONTROLLER_SIGNER.signingKey.mnemonic)
                console.log("\n DEPLOYER_SIGNER: ", this.DIRECTOR, "\n", this.DIRECTOR_SIGNER.signingKey.mnemonic)
                console.log("\n DEPLOYER_SIGNER: ", this.USER, "\n", this.USER_SIGNER.signingKey.mnemonic)
                console.log("erc820 => ", this.erc820.address)
                console.log("CapTableRegistryQue => ", this.stockQue.address)
                console.log("CapTableRegistry => ", this.stockRegistry.address)
                console.log("entityRegistry => ", this.entityRegistry.address)
                console.log("capTableSystemProxy => ", this.capTableSystemProxy.address)
            }
        });



        it('Update proxy address in proxy.ts', async () => {
            try {
                const chainId = (await provider.getNetwork).chainId
                if (!chainId) return

                const notNewProxy = "(?!" + this.PROXY_ADDRESS + ")"
                const regex = new RegExp("(5799: ')(" + notNewProxy + ".*){1}");
                const options = {
                    files: 'src/proxy.ts',
                    from: regex,
                    to: chainId + ": '" + this.PROXY_ADDRESS + "',",
                    countMatches: true,
                };
                const results = replace.sync(options);
                expect(results[0].hasChanged).to.be.true
            } catch (error) {
                console.log(error)
            }
        });



    });

})