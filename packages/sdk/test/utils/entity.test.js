// 'use strict'
// const expect = require('chai').expect;
// const assert = require('chai').assert;
// const R = require('ramda');
// const brregSdk = require('../../lib/index')
// const ethers = require('ethers')
// const randomData = require('../mock/data')
// const { getProvider } = require('./provider');
// const Deployer = require('@brreg/contracts').Deployer;

// const provider = getProvider(process.env.NETWORK || 'local')

// const { VALID_CERTIFICATE, PARTITION_A, PARTITION_B, WAIT_FOR_BLOCK_INCLUSION_TIME } = require('./constants');

// const isGanache = accounts => accounts.length >= 9;


// describe('Brønnøysundregisteret SDK', async function () {
//     this.timeout(WAIT_FOR_BLOCK_INCLUSION_TIME);
//     before("Setup accounts", async () => {
//         const totalAccounts = await provider.listAccounts()
//         if (totalAccounts.length >= 9) {
//             this.CONTROLLER1_SIGNER = provider.getSigner(0)
//             this.DIRECTOR_SIGNER = provider.getSigner(1) // CERTIFICATE_SIGNER
//             this.DEVELOPER_SIGNER = provider.getSigner(2)
//             this.NEW_DIRECTOR_SIGNER = provider.getSigner(3)
//             this.HOLDER_ONE_SIGNER = provider.getSigner(4)
//             this.HOLDER_TWO_SIGNER = provider.getSigner(5)
//             this.HOLDER_THREE_SIGNER = provider.getSigner(6)
//             this.FAUCET_SIGNER = provider.getSigner(7)
//             this.CERTIFICATE_SIGNER = this.DIRECTOR_SIGNER

//             this.CONTROLLER1 = await this.CONTROLLER1_SIGNER.getAddress()
//             this.DIRECTOR = await this.DIRECTOR_SIGNER.getAddress() // CERTIFICATE_SIGNER
//             this.DEVELOPER = await this.DEVELOPER_SIGNER.getAddress()
//             this.NEW_DIRECTOR = await this.NEW_DIRECTOR_SIGNER.getAddress()
//             this.HOLDER_ONE = await this.HOLDER_ONE_SIGNER.getAddress()
//             this.HOLDER_TWO = await this.HOLDER_TWO_SIGNER.getAddress()
//             this.HOLDER_THREE = await this.HOLDER_THREE_SIGNER.getAddress()
//             this.CERTIFICATE_SIGNER = await this.CERTIFICATE_SIGNER.getAddress()
//         } else {
//             this.CONTROLLER1_SIGNER = ethers.Wallet.createRandom().connect(provider)
//             this.DIRECTOR_SIGNER = ethers.Wallet.createRandom().connect(provider) // CERTIFICATE_SIGNER
//             this.DEVELOPER_SIGNER = ethers.Wallet.fromMnemonic('earth soldier purse ritual prize bird scrap repair basket shallow garden day').connect(provider)
//             this.NEW_DIRECTOR_SIGNER = ethers.Wallet.createRandom().connect(provider)
//             this.HOLDER_ONE_SIGNER = ethers.Wallet.createRandom().connect(provider)
//             this.HOLDER_TWO_SIGNER = ethers.Wallet.createRandom().connect(provider)
//             this.HOLDER_THREE_SIGNER = ethers.Wallet.createRandom().connect(provider)
//             this.FAUCET_SIGNER = ethers.Wallet.createRandom().connect(provider)
//             this.CERTIFICATE_SIGNER = this.DIRECTOR_SIGNER

//             this.CONTROLLER1 = await this.CONTROLLER1_SIGNER.getAddress()
//             this.DIRECTOR = await this.DIRECTOR_SIGNER.getAddress() // CERTIFICATE_SIGNER
//             this.DEVELOPER = await this.DEVELOPER_SIGNER.getAddress()
//             this.NEW_DIRECTOR = await this.NEW_DIRECTOR_SIGNER.getAddress()
//             this.HOLDER_ONE = await this.HOLDER_ONE_SIGNER.getAddress()
//             this.HOLDER_TWO = await this.HOLDER_TWO_SIGNER.getAddress()
//             this.HOLDER_THREE = await this.HOLDER_THREE_SIGNER.getAddress()
//             this.CERTIFICATE_SIGNER = await this.CERTIFICATE_SIGNER.getAddress()
//         }
//     })


//     before('setup deployer', () => {
//         this.deployer = new Deployer(this.DEVELOPER_SIGNER)
//     });

//     before('deploy erc820', async () => {
//         this.erc820 = await this.deployer.erc820()
//         //console.log("erc820 => ", this.erc820.address)
//         expect(this.erc820.address).to.be.string("")
//     });

//     before('deploy stockQue', async () => {
//         this.stockQue = await this.deployer.stockQue([this.CONTROLLER1])
//         expect(this.stockQue.address).to.be.string("")
//     });

//     before('deploy stockRegistry', async () => {
//         this.stockRegistry = await this.deployer.stockRegistry([this.CONTROLLER1])
//         expect(this.stockRegistry.address).to.be.string("")
//     });

//     before('deploy entityRegistry', async () => {
//         this.entityRegistryContract = await this.deployer.entityRegistry([this.CONTROLLER1])
//         expect(this.entityRegistryContract.address).to.be.string("")
//     });

//     before('deploy capTableSystemProxy', async () => {
//         this.capTableSystemProxy = await this.deployer.capTableSystemProxy([this.CONTROLLER1])
//         expect(this.capTableSystemProxy.address).to.be.string("")
//         // change to controller so we can set new contracts
//         this.capTableSystemProxy = this.capTableSystemProxy.connect(this.CONTROLLER1_SIGNER)
//         await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('erc820'), this.erc820.address)
//         await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('stockQue'), this.stockQue.address)
//         await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('stockRegistry'), this.stockRegistry.address)
//         await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('entityRegistry'), this.entityRegistryContract.address)
//         expect(await this.capTableSystemProxy.getContract(ethers.utils.formatBytes32String('erc820'))).to.be.equal(this.erc820.address)
//         //console.log("capTableSystemProxy => ", this.capTableSystemProxy.address)
//     });


//     it('SetupEntityRegistry', async () => {
//         let randomEntities = []
//         for (let i = 0; i < 8; i++) {
//             randomEntities.push(randomData.shareholder())
//         }
//         this.entityRegistry = await brregSdk.EntityRegistry.init(this.CONTROLLER1_SIGNER)

//         await this.entityRegistry.addEntity({ ...randomEntities[0], address: this.CONTROLLER1 });
//         await this.entityRegistry.addEntity({ ...randomEntities[1], address: this.DIRECTOR });
//         await this.entityRegistry.addEntity({ ...randomEntities[2], address: this.DEVELOPER });
//         await this.entityRegistry.addEntity({ ...randomEntities[3], address: this.NEW_DIRECTOR });
//         await this.entityRegistry.addEntity({ ...randomEntities[4], address: this.HOLDER_ONE });
//         await this.entityRegistry.addEntity({ ...randomEntities[5], address: this.HOLDER_TWO });
//         await this.entityRegistry.addEntity({ ...randomEntities[6], address: this.HOLDER_THREE });
//         await this.entityRegistry.addEntity({ ...randomEntities[7], address: this.CERTIFICATE_SIGNER });

//         this.CONTROLLER1_ENTITY = await this.entityRegistry.getEntityByAddress(this.CONTROLLER1)
//         this.DIRECTOR_ENTITY = await this.entityRegistry.getEntityByAddress(this.DIRECTOR)
//         this.DEVELOPER_ENTITY = await this.entityRegistry.getEntityByAddress(this.DEVELOPER)
//         this.NEW_DIRECTOR_ENTITY = await this.entityRegistry.getEntityByAddress(this.NEW_DIRECTOR)
//         this.HOLDER_ONE_ENTITY = await this.entityRegistry.getEntityByAddress(this.HOLDER_ONE)
//         this.HOLDER_TWO_ENTITY = await this.entityRegistry.getEntityByAddress(this.HOLDER_TWO)
//         this.HOLDER_THREE_ENTITY = await this.entityRegistry.getEntityByAddress(this.HOLDER_THREE)
//         this.CERTIFICATE_SIGNER_ENTITY = await this.entityRegistry.getEntityByAddress(this.CERTIFICATE_SIGNER)

//         expect(await this.entityRegistry.getEntityByAddress(this.DEVELOPER).uuid).to.equal((await this.entityRegistry.getEntityByUuid(this.DEVELOPER_ENTITY.uuid)).uuid)
//     })
// });
