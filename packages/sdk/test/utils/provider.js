const ethers = require('ethers');
const ganache = require('ganache-cli');

const getProvider = (network) => {
  switch (network) {
    case 'local':
      return new ethers.providers.JsonRpcProvider('HTTP://127.0.0.1:7545')
    case 'local-toyen':
      return new ethers.providers.JsonRpcProvider('HTTP://127.0.0.1:5799')
    case 'toyen':
      return new ethers.providers.JsonRpcProvider('http://ethjygmk4-dns-reg1.northeurope.cloudapp.azure.com:8540')
    case 'norchain':
      return new ethers.providers.JsonRpcProvider('http://dsop.northeurope.cloudapp.azure.com:8545')
    case 'cli':
      return new ethers.providers.Web3Provider(
        ganache.provider({
          "mnemonic": 'weapon stamp galaxy acquire copy ready soft pole depart tool task blind',
          "gasLimit": 50000000,
          "gasPrice": 0,
          "hardfork": "petersburg",
          "default_balance_ether": 0,
          "network_id": 5799,
          "locked": false,
          "port": 5799,
          "total_accounts": 1,
          "unlocked_accounts": [],
          "verbose": true,
          "vmErrorsOnRPCResponse": true
        })
      )
    default:
      break;
  }
};

module.exports = {
  getProvider
};
