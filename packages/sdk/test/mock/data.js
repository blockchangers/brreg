
var Chance = require('chance');
var chance = new Chance();
var ethers = require('ethers')

const company = () => {
    return {
        name: chance.company(),
        uuid: chance.string({ pool: '789', length: 9 }),
    }
}

const shareholder = () => {
    return {
        uuid: chance.birthday({ string: true, american: false }).split('/').join('') + chance.string({ pool: '23456', length: 5 }),
        type: 'person',
        name: chance.name(),
        country: 'Norway',
        city: 'Oslo',
        postalcode: chance.string({ pool: '0123456', length: 4 }),
        streetAddress: chance.address(),
        address: address()
    }
};

const address = () => {
    return ethers.Wallet.createRandom().address
}
module.exports = {
    company, shareholder, address
}
