'use strict'
const brregSdk = require('./../lib/index')
const Deployer = require('@brreg/contracts').Deployer;
const expect = require('chai').expect;
const ethers = require('ethers')
const randomData = require('./mock/data')
const { getProvider } = require('./utils/provider');

const provider = getProvider(process.env.NETWORK || 'cli' || 'local-toyen')

describe('SDK test', async function () {
    this.timeout(9000);
    it("Setup accounts", async () => {
        this.CONTROLLER_SIGNER = ethers.Wallet.createRandom().connect(provider)
        this.DIRECTOR_SIGNER = ethers.Wallet.createRandom().connect(provider)
        this.USER_SIGNER = ethers.Wallet.fromMnemonic('forget silent snap unlock federal elegant stool celery fetch salt draw cave').connect(provider)
        this.CONTROLLER = await this.CONTROLLER_SIGNER.getAddress()
        this.DIRECTOR = await this.DIRECTOR_SIGNER.getAddress()
        this.USER = await this.USER_SIGNER.getAddress()
    })

    it('Setup deployer', () => {
        this.deployer = new Deployer(this.CONTROLLER_SIGNER)
    });

    it('Deploy erc820', async () => {
        this.erc820 = await this.deployer.erc820()
        expect(this.erc820.address).to.be.string("")
    });

    it('Deploy stockQue', async () => {
        this.stockQue = await this.deployer.stockQue([this.CONTROLLER])
        expect(this.stockQue.address).to.be.string("")
    });

    it('Deploy stockRegistry', async () => {
        this.stockRegistry = await this.deployer.stockRegistry([this.CONTROLLER])
        expect(this.stockRegistry.address).to.be.string("")
    });

    it('Deploy entityRegistry', async () => {
        this.entityRegistryContract = await this.deployer.entityRegistry([this.CONTROLLER])
        expect(this.entityRegistryContract.address).to.be.string("")
    });

    it('Deploy capTableSystemProxy', async () => {
        this.capTableSystemProxy = await this.deployer.capTableSystemProxy([this.CONTROLLER])
        expect(this.capTableSystemProxy.address).to.be.string("")
        // console.log("capTableSystemProxy is", this.capTableSystemProxy)
        await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('erc820'), this.erc820.address, brregSdk.TX_OVERRIDES)
        await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('stockQue'), this.stockQue.address, brregSdk.TX_OVERRIDES)
        await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('stockRegistry'), this.stockRegistry.address, brregSdk.TX_OVERRIDES)
        await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('entityRegistry'), this.entityRegistryContract.address, brregSdk.TX_OVERRIDES)
        expect(await this.capTableSystemProxy.getContract(ethers.utils.formatBytes32String('erc820'))).to.be.equal(this.erc820.address)
        this.PROXY_ADDRESS = this.capTableSystemProxy.address
    });


    describe('EntityRegistry', async () => {

        it('Setup entities for accounts', async () => {
            this.entityRegistry = await brregSdk.EntityRegistry.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            const person1 = randomData.shareholder()
            const person2 = randomData.shareholder()
            const person3 = randomData.shareholder()
            await this.entityRegistry.addEntity({ ...person1, address: this.CONTROLLER });
            await this.entityRegistry.addEntity({ ...person2, address: this.DIRECTOR });
            await this.entityRegistry.addEntity({ ...person3, address: this.USER });
            this.CONTROLLER_ENTITY = await this.entityRegistry.getEntityByAddress(this.CONTROLLER);
            this.DIRECTOR_ENTITY = await this.entityRegistry.getEntityByAddress(this.DIRECTOR);
        });

    });

    describe('transactions and shareholders', async () => {

        it('Create SemiControllers company', async () => {
            const testCompany = randomData.company()
            const stockFactory = await brregSdk.StockFactory.init(this.DIRECTOR_SIGNER, this.PROXY_ADDRESS)
            this.SEMI_CONTROLLERS_COMPANY = await stockFactory.createNew("SemiControllers AS", testCompany.uuid)

            const stockQue = await brregSdk.RegistryOfCapTablesQue.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await stockQue.process(this.SEMI_CONTROLLERS_COMPANY.getAddress(), true, "Company CapTable approved by test.")

            const registry = await brregSdk.RegistryOfCapTables.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await registry.add(await this.SEMI_CONTROLLERS_COMPANY.getAddress())
        });

        it('que SemiControllers', async () => {
            const stockQue = await brregSdk.RegistryOfCapTablesQue.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await stockQue.process(this.SEMI_CONTROLLERS_COMPANY.getAddress(), true, "Company CapTable approved by test.")
        });

        it('process SemiControllers', async () => {

            const registry = await brregSdk.RegistryOfCapTables.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await registry.add(await this.SEMI_CONTROLLERS_COMPANY.getAddress())
        });

        it('create Transactions', async () => {
            this.entityRegistry = await brregSdk.EntityRegistry.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            const person1 = randomData.shareholder()
            const person2 = randomData.shareholder()
            await this.entityRegistry.addEntity({ ...person1 });
            await this.entityRegistry.addEntity({ ...person2 });

            await this.SEMI_CONTROLLERS_COMPANY.issue(person1.uuid, 1000);
            await this.SEMI_CONTROLLERS_COMPANY.issue(person2.uuid, 800);
            await this.SEMI_CONTROLLERS_COMPANY.issue(this.DIRECTOR_ENTITY.uuid, 600);
            await this.SEMI_CONTROLLERS_COMPANY.issue(this.CONTROLLER_ENTITY.uuid, 400);
            await this.SEMI_CONTROLLERS_COMPANY.issue(person2.uuid, 400, { partition: "B" });
            await this.SEMI_CONTROLLERS_COMPANY.issue(person1.uuid, 400, { partition: "B" });
            await this.SEMI_CONTROLLERS_COMPANY.issue(this.DIRECTOR_ENTITY.uuid, 400, { partition: "B" });
            await this.SEMI_CONTROLLERS_COMPANY.issue(this.CONTROLLER_ENTITY.uuid, 400, { partition: "B" });
            await this.SEMI_CONTROLLERS_COMPANY.operatorTransfer(person1.uuid, person2.uuid, 350)
            await this.SEMI_CONTROLLERS_COMPANY.redeem(50)
            await this.SEMI_CONTROLLERS_COMPANY.transfer(this.CONTROLLER_ENTITY.uuid, 250)
        });

        it('get Shareholders', async () => {
            const shareholders = await this.SEMI_CONTROLLERS_COMPANY.shareholders()
            expect(shareholders).length.greaterThan(1)
        });

        it('get Transactions', async () => {
            const transactions = await this.SEMI_CONTROLLERS_COMPANY.transactions()
            expect(transactions).length.greaterThan(4)
            const filteredTransactions = await this.SEMI_CONTROLLERS_COMPANY.transactions({ address: this.DIRECTOR })
            expect(filteredTransactions).length.lessThan(transactions.length)
        });

    });

    describe('Denomination', async () => {

        it('Create company', async () => {
            const testCompany = randomData.company()
            const stockFactory = await brregSdk.StockFactory.init(this.DIRECTOR_SIGNER, this.PROXY_ADDRESS)
            this.company1 = await stockFactory.createNew(testCompany.name, testCompany.uuid)

            const stockQue = await brregSdk.RegistryOfCapTablesQue.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await stockQue.process(this.company1.getAddress(), true, "Company CapTable approved by test.")

            const registry = await brregSdk.RegistryOfCapTables.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await registry.add(await this.company1.getAddress())
        });

        it('Set Denomination', async () => {
            await this.company1.setDenomination(5000);
            expect(await this.company1.denomination()).to.be.equal(5000)
        });

        it('Get correct denomination per share', async () => {
            await this.company1.issue(this.CONTROLLER_ENTITY.uuid, 2500);
            expect(await this.company1.denominationPerShare()).to.be.equal(2)
            await this.company1.issue(this.CONTROLLER_ENTITY.uuid, 7500);
            expect(await this.company1.denominationPerShare()).to.be.equal(0.5)
        });

    });

    describe('OperatorTransfer', async () => {

        it('Create company', async () => {
            const testCompany = randomData.company()
            const stockFactory = await brregSdk.StockFactory.init(this.DIRECTOR_SIGNER, this.PROXY_ADDRESS)
            this.SEMI_CONTROLLERS_COMPANY = await stockFactory.createNew("SemiControllers AS", testCompany.uuid)

            const stockQue = await brregSdk.RegistryOfCapTablesQue.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await stockQue.process(this.SEMI_CONTROLLERS_COMPANY.getAddress(), true, "Company CapTable approved by test.")

            const registry = await brregSdk.RegistryOfCapTables.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            await registry.add(await this.SEMI_CONTROLLERS_COMPANY.getAddress())
        });

        it('issue stocks to unManged accounts and transfer from them as operator', async () => {
            this.entityRegistry = await brregSdk.EntityRegistry.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS)
            const person1 = randomData.shareholder()
            const person2 = randomData.shareholder()
            await this.entityRegistry.addEntity({ ...person1 });
            await this.entityRegistry.addEntity({ ...person2 });

            await this.SEMI_CONTROLLERS_COMPANY.issue(person1.uuid, 1000);
            expect(await this.SEMI_CONTROLLERS_COMPANY.balanceOf(person1.address)).to.be.equal(1000)

            await this.SEMI_CONTROLLERS_COMPANY.operatorTransfer(person1.uuid, person2.uuid, 750)
            expect(await this.SEMI_CONTROLLERS_COMPANY.balanceOf(person1.address)).to.be.equal(250)
            expect(await this.SEMI_CONTROLLERS_COMPANY.balanceOf(person2.address)).to.be.equal(750)
        });

    });


    /* LATE TESTS */

    describe('CapTableRegistry', async () => {
        it('add and remove from capTableListRegistry', async () => {
            const testCompany = randomData.company()
            const stockFactory = await brregSdk.StockFactory.init(this.DIRECTOR_SIGNER, this.PROXY_ADDRESS)
            const RANDOM_COMPANY = await stockFactory.createNew(testCompany.name, testCompany.uuid)

            const capTableRegistry = await brregSdk.RegistryOfCapTables.init(this.CONTROLLER_SIGNER, this.PROXY_ADDRESS);

            let capTableListLength = (await capTableRegistry.list()).length
            await capTableRegistry.add(await RANDOM_COMPANY.getAddress());
            expect(capTableListLength).to.be.lessThan((await capTableRegistry.list()).length)
            await capTableRegistry.disable(await RANDOM_COMPANY.getAddress());
            expect(capTableListLength).to.be.equal((await capTableRegistry.list()).length)


        });
    });

    describe('director', async () => {
        it('should be able to transfer director role', async () => {
            const testCompany = randomData.company()
            const stockFactory = await brregSdk.StockFactory.init(this.DIRECTOR_SIGNER, this.PROXY_ADDRESS)
            const RANDOM_COMPANY = await stockFactory.createNew(testCompany.name, testCompany.uuid)

            const beforeDirector = await RANDOM_COMPANY.director();
            expect(beforeDirector).to.be.equal(this.DIRECTOR)
            await RANDOM_COMPANY.transferDirector(this.USER);
            const afterDirector = await RANDOM_COMPANY.director();
            expect(afterDirector).to.be.equal(this.USER);
        });
    });

    describe('erc820Registry', async () => {
        it('can i set new manager', async () => {
            const erc820Registry = await brregSdk.ERC820Registry.init(this.USER_SIGNER, this.PROXY_ADDRESS);
            const beforeManager = await erc820Registry.getManager(this.USER);
            expect(beforeManager, this.USER)
            await erc820Registry.setManager(this.USER, this.DIRECTOR);
            const afterManager = await erc820Registry.getManager(this.USER);
            expect(afterManager, this.DIRECTOR);
        });
    });

})
