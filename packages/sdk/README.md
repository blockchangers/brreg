# Documentation 💬
[Here](https://blockchangers.github.io/brreg-docs/#brreg-cap-table-brregcaptable-developer-documentation)

# Development
Make sure you have typescript installed.
`npm i typescript -g`

## Watch 🦓
`npm run watch` for Webpack to build js files into dist folder.