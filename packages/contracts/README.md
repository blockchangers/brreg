# Documentation 💬
[Here](https://blockchangers.github.io/brreg-docs/#brreg-cap-table-brregcaptable-developer-documentation)

# Development
Edit smart contracts in /src/contracts, then run `npm run compile` which will compile contracts to /src/compiled. This is then picked up by index.ts which packages these smart contracts as json ABI's for consumption elsewhere.
`npm i typescript -g`

## Test 🦓
`npm run test` or `npm run testw` for testing deloyment of contracts. Tests under /test/