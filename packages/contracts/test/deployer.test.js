const expect = require('chai').expect
const ethers = require('ethers')
const Deployer = require("../lib/index.js").Deployer
const ganache = require('ganache-cli');

const getProvider = (network) => {
    switch (network) {
        case 'local':
            return new ethers.providers.JsonRpcProvider('HTTP://127.0.0.1:7545')
        case 'local-toyen':
            return new ethers.providers.JsonRpcProvider('HTTP://127.0.0.1:5799')
        case 'toyen':
            return new ethers.providers.JsonRpcProvider('http://ethjygmk4-dns-reg1.northeurope.cloudapp.azure.com:8540')
        case 'norchain':
            return new ethers.providers.JsonRpcProvider('http://dsop.northeurope.cloudapp.azure.com:8545')
        case 'cli':
            return new ethers.providers.Web3Provider(
                ganache.provider({
                    "mnemonic": 'weapon stamp galaxy acquire copy ready soft pole depart tool task blind',
                    "gasLimit": 100000000,
                    "gasPrice": 0,
                    "hardfork": "petersburg",
                    "default_balance_ether": 0,
                    "network_id": 5799,
                    "locked": false,
                    "port": 5799,
                    "total_accounts": 1,
                    "unlocked_accounts": [],
                    "verbose": true,
                    "vmErrorsOnRPCResponse": true
                })
            )
        default:
            break;
    }
};
const provider = getProvider('norchain')

const TX_OVERRIDES = {
    // The price (in wei) per unit of gas
    gasPrice: ethers.utils.parseUnits('0.0', 'gwei'),
    //chainId: 6174
};

const VERBOSE = true

describe('brreg deploy', async function () {
    this.timeout(35000);
    it("Setup accounts", async () => {
        console.log("net", await provider.getNetwork())
        const totalAccounts = await provider.listAccounts()
        if (totalAccounts.length >= 9) {
            this.CONTROLLER_SIGNER = provider.getSigner(0)
            this.DEPLOYER_SIGNER = provider.getSigner(2)

            this.CONTROLLER = await this.CONTROLLER_SIGNER.getAddress()
            this.DEPLOYER = await this.DEPLOYER_SIGNER.getAddress()
        } else {
            this.CONTROLLER_SIGNER = ethers.Wallet.createRandom().connect(provider) //ethers.Wallet.fromMnemonic('fire fold cruise cook napkin clerk lyrics addict member shaft false myth').connect(provider)
            this.DEPLOYER_SIGNER = ethers.Wallet.createRandom().connect(provider) // ethers.Wallet.fromMnemonic('patch give alien defy river sock video blur aerobic sport range seed').connect(provider)

            this.CONTROLLER = await this.CONTROLLER_SIGNER.getAddress()
            this.DEPLOYER = await this.DEPLOYER_SIGNER.getAddress()

            console.log("CONTROLLER_SIGNER: ", this.CONTROLLER, "\n", this.CONTROLLER_SIGNER.signingKey.mnemonic)
            console.log("\n DEPLOYER_SIGNER: ", this.DEPLOYER, "\n", this.DEPLOYER_SIGNER.signingKey.mnemonic)

        }


    })

    it('setup deployer', () => {
        this.deployer = new Deployer(this.DEPLOYER_SIGNER)
    })

    it('deploy erc820', async () => {
        this.erc820 = await this.deployer.erc820()
        console.log("erc820 => ", this.erc820.address)
        expect(this.erc820.address).to.be.string("")
    })

    it('deploy stockQue', async () => {
        this.stockQue = await this.deployer.stockQue([this.CONTROLLER])
        console.log("stockQue =>", this.stockQue.address)
        expect(this.stockQue.address).to.be.string("")
    })

    it('deploy stockRegistry', async () => {
        this.stockRegistry = await this.deployer.stockRegistry([this.CONTROLLER])
        console.log("stockRegistry =>", this.stockRegistry.address)
        expect(this.stockRegistry.address).to.be.string("")
    })

    it('deploy entityRegistry', async () => {
        this.entityRegistry = await this.deployer.entityRegistry([this.CONTROLLER])
        console.log("entityRegistry =>", this.entityRegistry.address)
        expect(this.entityRegistry.address).to.be.string("")
    })

    it('deploy capTableSystemProxy', async () => {
        this.capTableSystemProxy = await this.deployer.capTableSystemProxy([this.CONTROLLER])
        console.log("capTableSystemProxy => ", this.capTableSystemProxy.address)
        expect(this.capTableSystemProxy.address).to.be.string("")

        // change to controller so we can set new contracts
        this.capTableSystemProxy = await this.capTableSystemProxy.connect(this.CONTROLLER_SIGNER) // TODO Sett inn overrides fra sdk.test

        let tx = await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('erc820'), this.erc820.address, TX_OVERRIDES)
        tx.wait()

        tx = await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('stockQue'), this.stockQue.address, TX_OVERRIDES)
        tx.wait()

        tx = await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('stockRegistry'), this.stockRegistry.address, TX_OVERRIDES)
        tx.wait()

        tx = await this.capTableSystemProxy.setContract(ethers.utils.formatBytes32String('entityRegistry'), this.entityRegistry.address, TX_OVERRIDES)
        tx.wait()

        // TODO Test doesn't work on Tøyen. Values are set, but checks fail (works manualy in Remix)
        // expect(await this.capTableSystemProxy.getContract(ethers.utils.formatBytes32String('erc820'))).to.be.equal(this.erc820.address, "")
        // expect(await this.capTableSystemProxy.getContract(ethers.utils.formatBytes32String('stockQue'))).to.be.equal(this.stockQue.address)
        // expect(await this.capTableSystemProxy.getContract(ethers.utils.formatBytes32String('stockRegistry'))).to.be.equal(this.stockRegistry.address)
        // expect(await this.capTableSystemProxy.getContract(ethers.utils.formatBytes32String('entityRegistry'))).to.be.equal(this.entityRegistry.address)
        if (VERBOSE) {
            console.log("CONTROLLER_SIGNER: ", this.CONTROLLER, "\n", this.CONTROLLER_SIGNER.signingKey.mnemonic)
            console.log("\n DEPLOYER_SIGNER: ", this.DEPLOYER, "\n", this.DEPLOYER_SIGNER.signingKey.mnemonic)
            console.log("erc820 => ", this.erc820.address)
            console.log("CapTableRegistryQue => ", this.stockQue.address)
            console.log("CapTableRegistry => ", this.stockRegistry.address)
            console.log("entityRegistry => ", this.entityRegistry.address)
            console.log("capTableSystemProxy => ", this.capTableSystemProxy.address)
        }
    })

});
