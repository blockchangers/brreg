// 'use strict'
// const assert = require('chai').assert
// const expect = require('chai').expect
// const ethers = require('ethers')
// const Contracts = require("../lib/index.js")
// const Deployer = require("../lib/index.js").Deployer

// const network = 'local'
// const waitForBlockInclusionTimeout = 20000
// const getProvider = () => {
//   switch (network) {
//     case 'local':
//       return new ethers.providers.JsonRpcProvider('HTTP://127.0.0.1:7545', 'unspecified')
//     case 'toyen':
//       return new ethers.providers.JsonRpcProvider('http://ethjygmk4-dns-reg1.northeurope.cloudapp.azure.com:8540', 'unspecified')
//     case 'cli':
//       const ganache = require('ganache-cli')
//       return new ethers.providers.Web3Provider(
//         ganache.provider({
//           mnemonic: 'weapon stamp galaxy acquire copy ready soft pole depart tool task blind',
//           gasLimit: '0x7A1200', // 8 000 000
//         }),
//         'unspecified'
//       )
//     default:
//       break;
//   }
// }
// const provider = getProvider()



// describe('brreg deploy', () => {

//   it("Setup accounts", async () => {
//     const totalAccounts = await provider.listAccounts()
//     if (totalAccounts.length >= 9) {
//       this.CONTROLLER1_SIGNER = provider.getSigner(0)
//       this.DIRECTOR_SIGNER = provider.getSigner(1) // CERTIFICATE_SIGNER
//       this.DEVELOPER_SIGNER = provider.getSigner(2)
//       this.NEW_DIRECTOR_SIGNER = provider.getSigner(3)
//       this.STOCK_HOLDER_ONE_SIGNER = provider.getSigner(4)
//       this.STOCK_HOLDER_TWO_SIGNER = provider.getSigner(5)
//       this.STOCK_HOLDER_THREE_SIGNER = provider.getSigner(6)
//       this.FAUCET_SIGNER = provider.getSigner(7)
//       this.CERTIFICATE_SIGNER = this.DIRECTOR_SIGNER

//       this.CONTROLLER1 = await this.CONTROLLER1_SIGNER.getAddress()
//       this.DIRECTOR = await this.DIRECTOR_SIGNER.getAddress() // CERTIFICATE_SIGNER
//       this.DEVELOPER = await this.DEVELOPER_SIGNER.getAddress()
//       this.NEW_DIRECTOR = await this.NEW_DIRECTOR_SIGNER.getAddress()
//       this.STOCK_HOLDER_ONE = await this.STOCK_HOLDER_ONE_SIGNER.getAddress()
//       this.STOCK_HOLDER_TWO = await this.STOCK_HOLDER_TWO_SIGNER.getAddress()
//       this.STOCK_HOLDER_THREE = await this.STOCK_HOLDER_THREE_SIGNER.getAddress()
//       this.CERTIFICATE_SIGNER = await this.CERTIFICATE_SIGNER.getAddress()
//     } else {
//       this.CONTROLLER1_SIGNER = ethers.Wallet.createRandom().connect(provider)
//       this.DIRECTOR_SIGNER = ethers.Wallet.createRandom().connect(provider) // CERTIFICATE_SIGNER
//       this.DEVELOPER_SIGNER = ethers.Wallet.fromMnemonic('earth soldier purse ritual prize bird scrap repair basket shallow garden day').connect(provider)
//       this.NEW_DIRECTOR_SIGNER = ethers.Wallet.createRandom().connect(provider)
//       this.STOCK_HOLDER_ONE_SIGNER = ethers.Wallet.createRandom().connect(provider)
//       this.STOCK_HOLDER_TWO_SIGNER = ethers.Wallet.createRandom().connect(provider)
//       this.STOCK_HOLDER_THREE_SIGNER = ethers.Wallet.createRandom().connect(provider)
//       this.FAUCET_SIGNER = ethers.Wallet.createRandom().connect(provider)
//       this.CERTIFICATE_SIGNER = this.DIRECTOR_SIGNER

//       this.CONTROLLER1 = await this.CONTROLLER1_SIGNER.getAddress()
//       this.DIRECTOR = await this.DIRECTOR_SIGNER.getAddress() // CERTIFICATE_SIGNER
//       this.DEVELOPER = await this.DEVELOPER_SIGNER.getAddress()
//       this.NEW_DIRECTOR = await this.NEW_DIRECTOR_SIGNER.getAddress()
//       this.STOCK_HOLDER_ONE = await this.STOCK_HOLDER_ONE_SIGNER.getAddress()
//       this.STOCK_HOLDER_TWO = await this.STOCK_HOLDER_TWO_SIGNER.getAddress()
//       this.STOCK_HOLDER_THREE = await this.STOCK_HOLDER_THREE_SIGNER.getAddress()
//       this.CERTIFICATE_SIGNER = await this.CERTIFICATE_SIGNER.getAddress()
//     }
//   }).timeout(waitForBlockInclusionTimeout)

//   it('setup deployer', () => {
//     this.deployer = new Deployer(this.DEVELOPER_SIGNER)
//   });


//   xit('deploy erc820', async () => {
//     this.erc820 = await this.deployer.erc820()
//   }).timeout(waitForBlockInclusionTimeout)

//   it('deploy testCompany1', async () => {
//     let data = {
//       CONTROLLERS: [this.CONTROLLER1],
//       CERTIFICATE_SIGNER: this.DIRECTOR,
//       PARTITIONS: ['0x4100000000000000000000000000000000000000000000000000000000000000'],
//     }

//     const stockFactory = new ethers.ContractFactory(Contracts.Stock.abi, Contracts.Stock.bytecode, this.CONTROLLER1_SIGNER)
//     this.stock = await stockFactory.deploy("TestCompany1", "TC1", 1, data.CONTROLLERS, data.CERTIFICATE_SIGNER, data.PARTITIONS, ethers.utils.formatBytes32String("12345678"))
//     await this.stock.deployed()

//     let code = await provider.getCode(this.stock.address)
//     assert.isTrue(code.length > 3, "Code deployed shhould be more the 3 characters");
//     //let receipt = await this.stock.deployTransaction.wait(1)
//     //console.log("Stock deployed => ", this.stock.address)
//   })

//   it('Deploy partition rule mock', async () => {
//     const partitionRuleMockFactory = new ethers.ContractFactory(Contracts.PartitionRuleMock.abi, Contracts.PartitionRuleMock.bytecode, this.CONTROLLER1_SIGNER)
//     this.partitionRuleMock = await partitionRuleMockFactory.deploy()

//     assert.isString(this.partitionRuleMock.address, "[partitionRuleMock address should be a string]");
//   });

//   it('should revert when issuing tokens with regular address as partition rule', async () => {
//     try {
//       await this.stock.issueByPartitionRule("0x4200000000000000000000000000000000000000000000000000000000000000", this.DIRECTOR, 500, "0x11", this.DEVELOPER)
//     } catch (error) {
//       assert.include(error.message, 'Partition rule must be contract address', "[Should revert when useing a non contract address for mock]");
//     }
//   });

//   it('should issue with partition rule', async () => {
//     await this.stock.issueByPartitionRule("0x4200000000000000000000000000000000000000000000000000000000000000", this.DIRECTOR, 500, "0x11", this.partitionRuleMock.address);
//     assert.equal((await this.stock.balanceOfByPartition("0x4200000000000000000000000000000000000000000000000000000000000000", this.DIRECTOR)).toString(10), 500, "Balanse after issue")

//   });

//   it('should be able to transfer stocks by partition', async () => {
//     this.stock = this.stock.connect(this.DIRECTOR_SIGNER)

//     await this.stock.transferByPartition(
//       "0x4200000000000000000000000000000000000000000000000000000000000000",
//       this.STOCK_HOLDER_ONE,
//       300,
//       "0x11"
//     )
//     assert.equal((await this.stock.balanceOfByPartition("0x4200000000000000000000000000000000000000000000000000000000000000", this.STOCK_HOLDER_ONE)).toString(10), 300, "Balanse after transfer")
//   });

//   it('issue a new partition without rule', async () => {
//     this.stock = this.stock.connect(this.CONTROLLER1_SIGNER)
//     const partitionB = "0x4300000000000000000000000000000000000000000000000000000000000000";
//     await this.stock.issueByPartition(partitionB, this.DIRECTOR, 8000, "0x11");
//     assert.equal((await this.stock.balanceOfByPartition(partitionB, this.DIRECTOR)).toString(10), 8000, "Balanse after issue")

//   });

// })
