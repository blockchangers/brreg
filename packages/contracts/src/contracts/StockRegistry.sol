pragma solidity >=0.4.24;

import "./StockRegistryInterface.sol";
import "./ControllerRole.sol";

contract StockRegistry is StockRegistryInterface, ControllerRole {
    address[] internal _stocks;
    mapping(address => bool ) internal _active;
    // Need this counter to determine how many active stocks to return in list
    uint256 internal _activeStocks;

    /* Events */
    event stockAdded(address stockAdded);
    event stockRemoved(address stockRemoved);


    /* Contructor */
    constructor(address[] controllers) ControllerRole(controllers) public {
        //
    }

    /* External  */

    function addStock(address stockAddress) external {
        _addStock(stockAddress);

    }

    function removeStock(address stockAddress) external {
        _removeStock(stockAddress);
    }

    function stockList() external view returns (address[]){
        require(_activeStocks > 0, "StockList cannot be empty");
        address[] memory stockAddressArray = new address[](_activeStocks);
        uint indexForStockAddressArray;
        for (uint i = 0; i < _stocks.length; i++){
            if( _active[_stocks[i]] ){
                stockAddressArray[indexForStockAddressArray] = _stocks[i];
                indexForStockAddressArray++;
            }
        }
        return stockAddressArray;
    }
    
    /* Internal */

    function _addStock(address stockAddress) internal onlyControllers{
        _stocks.push(stockAddress);
        _active[stockAddress] = true;
        _activeStocks++;
        emit stockAdded(stockAddress);
    }

    function _removeStock(address stockAddress) internal onlyControllers {
        _active[stockAddress] = false;
        _activeStocks--;
        emit stockRemoved(stockAddress);
    }
    
}