pragma solidity >=0.4.24;

interface StockQueInterface {
    event addressQued(address, uint256);
    event decision(address, bool, string);

    function addQue(address _stock) external ;
    function process(address _stock, bool _approved, string _reason) external;
}