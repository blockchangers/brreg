/* Not implementing full ERC820 comptability as we will later go over to ERC1820 */
pragma solidity >=0.4.24;

import "./PartitionRuleMockInterface.sol";

contract PartitionRuleMock is PartitionRuleMockInterface {

  function canTransfer(
    bytes32 /* partition */,
    address /* operator */,
    address /* from */,
    address /* to */,
    uint256 /* value */,
    bytes data,
    bytes /* operatorData */
    ) external view returns (bool){
    if(data.length > 0 && data[0] == hex"22") {
       require(false, "BRREG:  blocked - You must adhere to the drag along rights");
       return false;
     } else {
       return true;
     }
    }


}
