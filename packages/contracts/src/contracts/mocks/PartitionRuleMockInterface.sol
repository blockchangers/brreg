pragma solidity >=0.4.24;

interface PartitionRuleMockInterface {

  function canTransfer(
    bytes32 partition,
    address operator,
    address from,
    address to,
    uint256 value,
    bytes data,
    bytes operatorData
    ) external view returns (bool);
}
