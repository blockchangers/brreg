pragma solidity >=0.4.24;

interface StockInterface {
  function uuid() external view returns(bytes32);
  function setUuid(bytes32 newUuid) external;
  function transferDirector(address newCEO) external;
  function getDirector() external view returns (address);
}