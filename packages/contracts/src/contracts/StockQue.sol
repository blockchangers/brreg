pragma solidity >=0.4.24;

import "./StockQueInterface.sol";
import "./ControllerRole.sol";

contract StockQue is StockQueInterface, ControllerRole {
    address[] internal _que;
    mapping(uint256 => bool) internal _queNumberIsProcessed;
    mapping(address => uint256) internal _addressToQueNumber;
    event addressQued(address, uint256);
    event decision(address, bool, string);

    constructor(address[] controllers) ControllerRole(controllers) public {
        // Init the que so we can have 0 represent not qued
        // uint256 queIndex = _que.push(msg.sender) - 1;
        // _queNumberIsProcessed[queIndex] = true;
        // _addressToQueNumber[msg.sender] = queIndex;
    }

    function addQue(address _address) external {
        require(_address != address(0), "No empty address");
        uint256 queIndex = _que.push(_address) - 1;
        _queNumberIsProcessed[queIndex] = false;
        _addressToQueNumber[_address] = queIndex;
        emit addressQued(_address, queIndex);
    }

    function process(address _address, bool _approved, string _reason) external onlyControllers{
        require(_address != address(0), "No empty address");
        _queNumberIsProcessed[_queNumber(_address)] = true;
        emit decision(_address, _approved, _reason);
    }

    function isQued(address _address) external view returns(bool){
        uint256 queNumber = _queNumber(_address);
        return (_queNumberIsProcessed[queNumber] == false );
    }

    function isProcessed(address _address) external view returns(bool){
        return _queNumberIsProcessed[_queNumber(_address)];
    }
    
    function que() external view returns(address[]) {
        return _que;
    }

    function queNumber(address _address) external view returns(uint256){
        return _queNumber(_address);
    }
    function _queNumber(address _address) internal view returns(uint256){
        return _addressToQueNumber[_address];
    }
}