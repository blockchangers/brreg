pragma solidity >=0.4.24;

import "./ControllerRole.sol";
import "./CapTableSystemProxyInterface.sol";

contract CapTableSystemProxy is CapTableSystemProxyInterface, ControllerRole {
    mapping (bytes32 => address) internal _contract;
    mapping(address => bool) internal _activeController;
    address[] internal _controllers;
    uint256 internal _activeControllerCount;

    /* Contructor */
    constructor( address[] controllers ) ControllerRole(controllers) public {
        for(uint i = 0; i < controllers.length; i++){
            _controllers.push(controllers[i]);
            _activeControllerCount++;
        }
    }

    function setContract(bytes32 name, address contractAddress) external onlyControllers {
        _contract[name] = contractAddress;
    }

    function getContract(bytes32 name) external view returns (address){
        return _contract[name];
    }

    /* Overrides */
    function addController(address newController) external onlyControllers{
        CONTROLLER.add(newController);
        _controllers.push(newController);
        _activeControllerCount++;
        emit controllerAdded(newController);
    }

    /* Overrides */
    function removeController(address controllerToRemove) external onlyControllers {
        CONTROLLER.remove(controllerToRemove);
        _activeControllerCount--;
        emit controllerRemoved(controllerToRemove);
    }

    function listControllers() external view returns (address[]){
        address[] memory addressArray = new address[](_activeControllerCount);
        uint indexAddressArray;
        for (uint i = 0; i < _controllers.length ; i++){
            if(CONTROLLER.has(_controllers[i])){
                addressArray[indexAddressArray] = _controllers[i];
                indexAddressArray++;
            }
        }
        return addressArray;
    }
}