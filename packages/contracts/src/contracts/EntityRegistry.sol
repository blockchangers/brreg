pragma solidity >=0.4.24;

import "./ControllerRole.sol";

contract EntityRegistry is ControllerRole {

    /* Variables */
    Entity[] internal _entities;
    mapping(address => uint256) internal _addressToEntity;
    mapping(bytes32 => uint256) internal _uuidToEntity;
    mapping(bytes32 => address) internal _uuidToAddress;

    /* Events */

    event entityAdded
    (
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streetAddress
    );

    event entityUpdated
    (
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streetAddress
    );

    struct Entity {
        bytes32 uuid;
        uint entityType;
        bytes32 name;
        bytes32 country;
        bytes32 city;
        bytes32 postalcode;
        bytes32 streetAddress;
    }

    /* Contructor */
    constructor(address[] controllers) ControllerRole(controllers) public {
        Entity memory entity = Entity({
            uuid: 0x0000000000000000000000000000000000000000000000000000000000000000,
            entityType: 0,
            name: 0x0000000000000000000000000000000000000000000000000000000000000000,
            country: 0x0000000000000000000000000000000000000000000000000000000000000000,
            city: 0x0000000000000000000000000000000000000000000000000000000000000000,
            postalcode: 0x0000000000000000000000000000000000000000000000000000000000000000,
            streetAddress: 0x0000000000000000000000000000000000000000000000000000000000000000
        });
        _entities.push(entity) - 1;
    }

    function updateEntity(
        address addressForEntity,
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streetAddress
    ) external /* onlyControllers in test we allow anyone to create entity*/ {
        Entity memory entity = Entity({
            uuid: uuid,
            entityType: entityType,
            name: name,
            country: country,
            city: city,
            postalcode: postalcode,
            streetAddress: streetAddress
        });
        require(uuid[0] != 0, "Entity uuid must not be empty");
        require(name[0] != 0, "Entity name must not be empty");
        require(streetAddress[0] != 0, "Entity streetAddress must not be empty");
        require(postalcode[0] != 0, "Entity postalcode must not be empty");
        uint256 entityId = _entities.push(entity) - 1;
        _addressToEntity[addressForEntity] = entityId;
        _uuidToAddress[uuid] = addressForEntity;
        _uuidToEntity[uuid] = entityId;
        emit entityUpdated
        (
            uuid,
            entityType,
            name,
            country,
            city,
            postalcode,
            streetAddress
        );
    }

    /**
    * [NOT MANDATORY FOR ANY STANDARD]
    * @dev Get universally unique identifier for stock
    * @return The universally unique identifier
    */
    function addEntity(
        address addressForEntity,
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streetAddress
        ) external /* onlyControllers in test we allow anyone to create entity*/ {
            require(uuid[0] != 0, "Entity uuid must not be empty");
            require(name[0] != 0, "Entity name must not be empty");
            require(streetAddress[0] != 0, "Entity streetAddress must not be empty");
            require(postalcode[0] != 0, "Entity postalcode must not be empty");
            Entity memory entity = Entity({
                uuid: uuid,
                entityType: entityType,
                name: name,
                country: country,
                city: city,
                postalcode: postalcode,
                streetAddress: streetAddress
            });
            uint256 entityId = _entities.push(entity) - 1;
            _addressToEntity[addressForEntity] = entityId;
            _uuidToAddress[uuid] = addressForEntity;
            _uuidToEntity[uuid] = entityId;
            emit entityAdded
            (
                uuid,
                entityType,
                name,
                country,
                city,
                postalcode,
                streetAddress
            );
        }

    function getEntity( uint256 entityId) external view returns (
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streetAddress
        ) {
            require(entityId != 0, "Entity does not exist.");
            Entity memory entity = _entities[entityId];
            return (entity.uuid, entity.entityType, entity.name, entity.country, entity.city, entity.postalcode, entity.streetAddress);
    }

    function getEntityByAddress(address addressForEntity) external view returns (
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streetAddress
        ) {
            uint256 entityId = _addressToEntity[addressForEntity];
            return this.getEntity(entityId);
    }
    
    function getEntityByUuid(bytes32 _uuid) external view returns(
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streetAddress
    ) {
        uint256 entityId = _uuidToEntity[_uuid];
        return this.getEntity(entityId);
    }

    function getAddressByUuid(bytes32 uuid) external view returns (
        address addressForEntity
        ) {
            return _uuidToAddress[uuid];
    }
}