pragma solidity >=0.4.24;

contract StockRegistryInterface {
    event stockAdded(address stockAdded);
    event stockRemoved(address stockRemoved);
    function addStock(address stockAddress) external;
    function removeStock(address stockAddress) external ;
    function stockList() external view returns (address[]);
}