pragma solidity >=0.4.24;

interface CapTableSystemProxyInterface {
    function setContract(bytes32 name, address contractAddress) external;
    function getContract(bytes32 name) external view returns (address);
    function listControllers() external view returns (address[]);
}