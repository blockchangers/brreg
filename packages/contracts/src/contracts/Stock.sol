pragma solidity >=0.4.24;

import "./token/ERC20/ERC1400ERC20.sol";
import "./StockInterface.sol";
import "./mocks/PartitionRuleMockInterface.sol";

contract Stock is StockInterface, ERC1400ERC20 {
    /* Variables */
    using Roles for Roles.Role;
    Roles.Role private DIRECTOR;
    address _director;
    bytes32 internal _uuid;
    uint256 internal _denomination;

    
   /******************** Mapping for partition rule - BRREG **************************/
  // Mapping from partition to partition rule implementer. [PARTITION-SPECIFIC]
  mapping(bytes32 => address) internal _partitionRuleAddress;
  /****************************************************************************/

    constructor(
    string name,
    string symbol,
    uint256 granularity,
    address[] controllers,
    address certificateSigner,
    bytes32[] tokenDefaultPartitions,
    bytes32 uuid,
    address erc820client
  )
    public
    ERC1400ERC20(name, symbol, granularity, controllers, certificateSigner, tokenDefaultPartitions, erc820client)
  {
      _uuid = uuid;
      DIRECTOR.add(certificateSigner);
      _director = certificateSigner;
      _denomination = 0;
  }

  function denomination() external view returns (uint256) {
    return _denomination;
  }

  function setDenomination(uint256 newDenomination) external {
    require(DIRECTOR.has(msg.sender) || _isController[msg.sender], "msg.sender must be DIRECTOR or CONTROLLER");
    _denomination = newDenomination;
  }

  function isController(address _address) external view returns(bool) {
    return _isController[_address];
  }



  /**
   * [NOT MANDATORY FOR ANY STANDARD]
   * @dev Get universally unique identifier for stock
   * @return The universally unique identifier
   */
  function uuid() external view returns(bytes32) {
    return _uuid;
  }

  /**
   * [NOT MANDATORY FOR ANY STANDARD]
   * @dev Set universally unique identifier for stock
   * @param newUuid The new uuid to set.
   * @return void
   */
  function setUuid(bytes32 newUuid) external{
    require(_isController[msg.sender], "msg.sender must be CONTROLLER");
    _uuid = newUuid;
  }

  /**
   * [NOT MANDATORY FOR ANY STANDARD]
   * @dev Get universally unique identifier for stock. Current DIRECTOR and controllers are able to do this.
   * @param _newDirector Address of the newly appointed DIRECTOR. 
   * @return The universally unique identifier
   */
  function transferDirector(address _newDirector) external {
    require(DIRECTOR.has(msg.sender) || _isController[msg.sender], "msg.sender must be DIRECTOR or CONTROLLER");
    DIRECTOR.add(_newDirector);
    _addMinter(_newDirector);
    DIRECTOR.remove(msg.sender);
    _removeMinter(msg.sender);
    _director = _newDirector;
  }

  function getDirector() external view returns (address) {
    return _director;
  }

  function issueByPartitionRule(bytes32 partition, address tokenHolder, uint256 value, bytes data, address partitionRuleAddress)
    external
    onlyMinter
    issuableToken
    isValidCertificate(data)
  {
    _setPartitionRule(partition, partitionRuleAddress);
    _issueByPartition(partition, msg.sender, tokenHolder, value, data, "");
  }

  function _setPartitionRule(bytes32 partition, address partitionRuleAddress) internal {
    require(!_isRegularAddress(partitionRuleAddress), "BRREG: Issue blocked - Partition rule must be contract address");
    _partitionRuleAddress[partition] = partitionRuleAddress;
  }

  function _callPartitionRule(
      bytes32 partition,
      address operator,
      address from,
      address to,
      uint256 value,
      bytes data,
      bytes operatorData
    ) 
    internal view returns(bool){
    address partitionRuleImplementation;
    partitionRuleImplementation = PartitionRuleMockInterface(_partitionRuleAddress[partition]);
    bool returnVal = false;
    if (partitionRuleImplementation != address(0)) {
      returnVal = PartitionRuleMockInterface(partitionRuleImplementation).canTransfer(partition, operator, from, to, value, data, operatorData);
    }
    return returnVal;
  }

  /* Override */
  function _transferWithData(
    bytes32 partition,
    address operator,
    address from,
    address to,
    uint256 value,
    bytes data,
    bytes operatorData,
    bool preventLocking
  )
    internal
    nonReentrant
  {
    require(_isMultiple(value), "A9: Transfer Blocked - Token granularity");
    require(to != address(0), "A6: Transfer Blocked - Receiver not eligible");
    require(_balances[from] >= value, "A4: Transfer Blocked - Sender balance insufficient");

    _callSender(partition, operator, from, to, value, data, operatorData);

    /* BRREG SPECIFIC START */
    _callPartitionRule(partition, operator, from, to, value, data, operatorData);
    /* BRREG SPECIFIC END */

    _balances[from] = _balances[from].sub(value);
    _balances[to] = _balances[to].add(value);

    _callRecipient(partition, operator, from, to, value, data, operatorData, preventLocking);

    emit TransferWithData(operator, from, to, value, data, operatorData);
  }

  

}