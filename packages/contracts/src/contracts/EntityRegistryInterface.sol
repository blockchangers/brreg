pragma solidity >=0.4.24;

interface EntityRegistryInterface {

    event entityAdded
    (
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streedAddress
    );

    function addEntity(
        address addressForEntity,
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streedAddress
        ) external ;

    function getEntity( uint256 entityId) external view returns (
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streedAddress
        );

    function getEntityByAddress(address addressForEntity) external view returns (
        bytes32 uuid,
        uint entityType,
        bytes32 name,
        bytes32 country,
        bytes32 city,
        bytes32 postalcode,
        bytes32 streedAddress
        );

}