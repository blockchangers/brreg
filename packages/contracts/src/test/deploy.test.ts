import { Provider, JsonRpcProvider, JsonRpcSigner } from "ethers/providers";
import { ethers, Wallet } from "ethers";
import { Deployer } from './../index'


const ganacheCli = require('ganache-cli')


// let provider: Provider | JsonRpcProvider;
const provider = new ethers.providers.Web3Provider(
    ganacheCli.provider({
        mnemonic: 'weapon stamp galaxy acquire copy ready soft pole depart tool task blind',
        gasLimit: '0x7A1200', // 8 000 000
    }),
    'unspecified'
)
const waitForBlockInclusionTimeout = 3000

// signers
let _CONTROLLER1_SIGNER: (Wallet | JsonRpcSigner)
let _DIRECTOR_SIGNER: (Wallet | JsonRpcSigner)
let _DEVELOPER_SIGNER: (Wallet | JsonRpcSigner)
let _NEW_DIRECTOR_SIGNER: (Wallet | JsonRpcSigner)
let _STOCK_HOLDER_ONE_SIGNER: (Wallet | JsonRpcSigner)
let _STOCK_HOLDER_TWO_SIGNER: (Wallet | JsonRpcSigner)
let _STOCK_HOLDER_THREE_SIGNER: (Wallet | JsonRpcSigner)
let _FAUCET_SIGNER: (Wallet | JsonRpcSigner)

// Addresses
let _CONTROLLER1_ADDRESS: string
let _DIRECTOR_ADDRESS: string
let _DEVELOPER_ADDRESS: string
let _NEW_DIRECTOR_ADDRESS: string
let _STOCK_HOLDER_ONE_ADDRESS: string
let _STOCK_HOLDER_TWO_ADDRESS: string
let _STOCK_HOLDER_THREE_ADDRESS: string
let _FAUCET_ADDRESS: string

// const initProvider = (network?: string): Provider | JsonRpcProvider => {
//     if (network === 'toyen') {
//         return new ethers.providers.JsonRpcProvider('https://ethm2ricd-dns-reg1.northeurope.cloudapp.azure.com', 'unspecified')
//     }
//     if (network === 'local') {
//         return new ethers.providers.JsonRpcProvider('HTTP://127.0.0.1:7545', 'unspecified')
//     }
//     if (network !== undefined) {
//         return new ethers.providers.JsonRpcProvider(network, 'unspecified')
//     }
//     // Defaults too ganache cli if no network defined
//     return new ethers.providers.Web3Provider(
//         ganacheCli.provider({
//             mnemonic: 'weapon stamp galaxy acquire copy ready soft pole depart tool task blind',
//             gasLimit: '0x7A1200', // 8 000 000
//         }),
//         'unspecified'
//     )
// }

// const initAccounts = async () => {
//     const getAccount = (provider: Provider, n: number): (Wallet | JsonRpcSigner) => {
//         if (provider instanceof JsonRpcProvider && provider.listAccounts.length >= n) {
//             return provider.getSigner(n)
//         } else {
//             const wallet = ethers.Wallet.createRandom()
//             return wallet.connect(provider)
//         }
//     }

//     _CONTROLLER1_SIGNER = getAccount(provider, 0);
//     _DIRECTOR_SIGNER = getAccount(provider, 1);
//     _DEVELOPER_SIGNER = getAccount(provider, 2);
//     _NEW_DIRECTOR_SIGNER = getAccount(provider, 3);
//     _STOCK_HOLDER_ONE_SIGNER = getAccount(provider, 4);
//     _STOCK_HOLDER_TWO_SIGNER = getAccount(provider, 5);
//     _STOCK_HOLDER_THREE_SIGNER = getAccount(provider, 6);
//     _FAUCET_SIGNER = getAccount(provider, 6);

//     _CONTROLLER1_ADDRESS = await _CONTROLLER1_SIGNER.getAddress()
//     _DIRECTOR_ADDRESS = await _DIRECTOR_SIGNER.getAddress()
//     _DEVELOPER_ADDRESS = await _DEVELOPER_SIGNER.getAddress()
//     _NEW_DIRECTOR_ADDRESS = await _NEW_DIRECTOR_SIGNER.getAddress()
//     _STOCK_HOLDER_ONE_ADDRESS = await _STOCK_HOLDER_ONE_SIGNER.getAddress()
//     _STOCK_HOLDER_TWO_ADDRESS = await _STOCK_HOLDER_TWO_SIGNER.getAddress()
//     _STOCK_HOLDER_THREE_ADDRESS = await _STOCK_HOLDER_THREE_SIGNER.getAddress()
//     _FAUCET_ADDRESS = await _FAUCET_SIGNER.getAddress()
// }



beforeAll(async () => {
    // provider = initProvider();
    // initAccounts();
    const totalAccounts = await provider.listAccounts()
    if (totalAccounts.length >= 9) {
        _CONTROLLER1_SIGNER = provider.getSigner(0)
        _DIRECTOR_SIGNER = provider.getSigner(1) // CERTIFICATE_SIGNER
        _DEVELOPER_SIGNER = provider.getSigner(2)
        _NEW_DIRECTOR_SIGNER = provider.getSigner(3)
        _STOCK_HOLDER_ONE_SIGNER = provider.getSigner(4)
        _STOCK_HOLDER_TWO_SIGNER = provider.getSigner(5)
        _STOCK_HOLDER_THREE_SIGNER = provider.getSigner(6)
        _FAUCET_SIGNER = provider.getSigner(7)


        _CONTROLLER1_ADDRESS = await _CONTROLLER1_SIGNER.getAddress()
        _DIRECTOR_ADDRESS = await _DIRECTOR_SIGNER.getAddress() // CERTIFICATE_SIGNER
        _DEVELOPER_ADDRESS = await _DEVELOPER_SIGNER.getAddress()
        _NEW_DIRECTOR_ADDRESS = await _NEW_DIRECTOR_SIGNER.getAddress()
        _STOCK_HOLDER_ONE_ADDRESS = await _STOCK_HOLDER_ONE_SIGNER.getAddress()
        _STOCK_HOLDER_TWO_ADDRESS = await _STOCK_HOLDER_TWO_SIGNER.getAddress()
        _STOCK_HOLDER_THREE_ADDRESS = await _STOCK_HOLDER_THREE_SIGNER.getAddress()
    } else {
        _CONTROLLER1_SIGNER = ethers.Wallet.createRandom().connect(provider)
        _DIRECTOR_SIGNER = ethers.Wallet.createRandom().connect(provider)
        _DEVELOPER_SIGNER = ethers.Wallet.createRandom().connect(provider)
        _NEW_DIRECTOR_SIGNER = ethers.Wallet.createRandom().connect(provider)
        _STOCK_HOLDER_ONE_SIGNER = ethers.Wallet.createRandom().connect(provider)
        _STOCK_HOLDER_TWO_SIGNER = ethers.Wallet.createRandom().connect(provider)
        _STOCK_HOLDER_THREE_SIGNER = ethers.Wallet.createRandom().connect(provider)
        _FAUCET_SIGNER = ethers.Wallet.createRandom().connect(provider)


        _CONTROLLER1_ADDRESS = await _CONTROLLER1_SIGNER.getAddress()
        _DIRECTOR_ADDRESS = await _DIRECTOR_SIGNER.getAddress()
        _DEVELOPER_ADDRESS = await _DEVELOPER_SIGNER.getAddress()
        _NEW_DIRECTOR_ADDRESS = await _NEW_DIRECTOR_SIGNER.getAddress()
        _STOCK_HOLDER_ONE_ADDRESS = await _STOCK_HOLDER_ONE_SIGNER.getAddress()
        _STOCK_HOLDER_TWO_ADDRESS = await _STOCK_HOLDER_TWO_SIGNER.getAddress()
        _STOCK_HOLDER_THREE_ADDRESS = await _STOCK_HOLDER_THREE_SIGNER.getAddress()
    }


    console.log(_CONTROLLER1_ADDRESS);
    console.log(_DEVELOPER_ADDRESS);
});

test('should deploy erc820', async () => {

    let ERC820RegistryContractAsDeveloper = await (new Deployer(_DEVELOPER_SIGNER)).erc820()

    expect(await ERC820RegistryContractAsDeveloper.getManager(_DEVELOPER_ADDRESS)).toEqual(_DEVELOPER_ADDRESS)
    console.log('erc820registry deployed => ', ERC820RegistryContractAsDeveloper.address)
})

test('basic', () => {
    expect(0).toBe(0);
});

test('basic again', () => {
    expect(3).toBe(3);
});