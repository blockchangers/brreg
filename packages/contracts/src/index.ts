import * as Stock from "./compiled/Stock.json"
import * as StockQue from "./compiled/StockQue.json"
import * as StockRegistry from "./compiled/StockRegistry.json"
import * as EntityRegistry from "./compiled/EntityRegistry.json"
import * as ERC820Registry from "./compiled/ERC820RegistryDeployable.json"
import * as PartitionRuleMock from "./compiled/PartitionRuleMock.json"
import * as CapTableSystemProxy from "./compiled/CapTableSystemProxy.json"
import { Deployer } from './Deployer'

export { Deployer, Stock, StockQue, StockRegistry, EntityRegistry, ERC820Registry, PartitionRuleMock, CapTableSystemProxy }
