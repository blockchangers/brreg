import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.view.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'About',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.view.vue')
    },
    {
      path: '/RegistryOfCapTables',
      name: 'RegistryOfCapTables',
      component: () => import(/* webpackChunkName: "RegistryOfCapTables" */ './views/RegistryOfCapTables.view.vue')
    },
    {
      path: '/CapTable/create',
      name: 'CapTableCreate',
      component: () => import(/* webpackChunkName: "CapTableCreate" */ './views/CapTableCreate.view.vue')
    },
    {
      path: '/CapTable/:id',
      name: 'CapTable',
      component: () => import(/* webpackChunkName: "CapTable" */ './views/CapTable.view.vue')
    },
    {
      path: '/RegistryOfCapTablesQue',
      name: 'RegistryOfCapTablesQue',
      component: () => import(/* webpackChunkName: "RegistryOfCapTablesQue" */ './views/RegistryOfCapTablesQue.view.vue')
    },
    {
      path: '/entity/list',
      name: 'EntityList',
      component: () => import(/* webpackChunkName: "EntityList" */ './views/EntityList.view.vue')
    },
    {
      path: '/entity/create',
      name: 'EntityCreate',
      component: () => import(/* webpackChunkName: "EntityCreate" */ './views/EntityCreate.view.vue')
    },
    {
      path: '/entity/:id',
      name: 'Entity',
      component: () => import(/* webpackChunkName: "EntityView" */ './views/Entity.view.vue')
    },

  ]
})
