let ethereum
if (typeof window.ethereum !== 'undefined' || (typeof window.web3 !== 'undefined')) {
    ethereum = window.ethereum
}

const registryOfCapTablesQue = require("@brreg/sdk").RegistryOfCapTablesQue
import Vue from "vue";
export default {
    state: {
        que: []
    },
    getters: {},
    actions: {
        async init({ rootState, dispatch }) {
            if (!rootState.metamask.initialized) {
                dispatch('metamask/init', {}, { root: true })
                // setTimeout(() => {
                //     dispatch('init')
                // }, 1000)
            }
            dispatch('list')

        },
        async getQue({ commit }) {
            const registryOfCapTablesQueContract = await registryOfCapTablesQue.init(ethereum)
            console.log("getting que")
            const que = await registryOfCapTablesQueContract.que()
            console.log(que)
            commit('que', { que: que })
            return que
        },
        async proccess({ commit }) {
            const registryOfCapTablesQueContract = await registryOfCapTablesQue.init(ethereum)
            return await registryOfCapTablesQueContract.process()
        }
    },
    mutations: {
        que(state, { que }) {
            Vue.set(state, 'que', que)
        }
    },
    namespaced: true
}