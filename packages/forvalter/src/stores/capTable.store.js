const StockFactory = require("@brreg/sdk").StockFactory;
const Stock = require("@brreg/sdk").Stock;
const RegistryOfCapTablesQue = require("@brreg/sdk").RegistryOfCapTablesQue;
const RegistryOfCapTables = require("@brreg/sdk").RegistryOfCapTables;

let ethereum
if (typeof window.ethereum !== 'undefined' || (typeof window.web3 !== 'undefined')) {
    ethereum = window.ethereum
}
export default {
    state: {
    },
    mutations: {},
    actions: {
        async createNew({ dispatch }, { name, uuid }) {
            const stockFactory = await StockFactory.init(ethereum)
            return stockFactory.createNew(name, uuid)
        },
        async capTable({ }, { id }) {
            return await Stock.init(ethereum, id)
        },
        async operatorTransfer({ }, { id, fromUuid, toUuid, amount }) {
            const capTable = await Stock.init(ethereum, id)
            capTable.operatorTransfer(fromUuid, toUuid, amount)
        },
        async issue({ }, { id, toUuid, amount, partition }) {
            console.log(id, toUuid, amount, partition)
            if (partition === 'standard' || partition === "") {
                partition = undefined
            }
            const capTable = await Stock.init(ethereum, id)
            await capTable.issue(toUuid, amount, { partition: partition })
        },
        async getInfo({ }, { id }) {
            const capTable = await Stock.init(ethereum, id)
            return await capTable.info()
        },
        async getShareholders({ }, { id }) {
            const capTable = await Stock.init(ethereum, id)
            const shareholders = await capTable.shareholders()
            console.log("shareholders", shareholders)
            return shareholders
        },
        async getTransactions({ }, { id }) {
            const capTable = await Stock.init(ethereum, id)
            const transactions = await capTable.transactions()
            console.log("transactions", transactions)
            return transactions
        },
        async getQueInfo({ }, { id }) {
            const registryOfCapTablesQue = await RegistryOfCapTablesQue.init(ethereum)
            const isQued = await registryOfCapTablesQue.isQued(id)
            const queNumber = await registryOfCapTablesQue.queNumber(id)
            return {
                queNumber,
                isQued,
                address: id
            }
        },
        async processQue({ }, { id, approved, reason }) {
            const registryOfCapTablesQue = await RegistryOfCapTablesQue.init(ethereum)
            const process = await registryOfCapTablesQue.process(id, approved, reason)
            const registryOfCapTables = await RegistryOfCapTables.init(ethereum)
            const add = await registryOfCapTables.add(id)
            return
        }
    },
    namespaced: true
};
