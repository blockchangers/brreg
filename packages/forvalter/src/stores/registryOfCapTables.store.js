let ethereum
if (typeof window.ethereum !== 'undefined' || (typeof window.web3 !== 'undefined')) {
    ethereum = window.ethereum
}

const RegistryOfCapTables = require("@brreg/sdk").RegistryOfCapTables
import Vue from "vue";
export default {
    state: {
        capTables: []
    },
    getters: {},
    actions: {
        async init({ rootState, dispatch }) {
            if (!rootState.metamask.initialized) {
                dispatch('metamask/init', {}, { root: true })
                // setTimeout(() => {
                //     dispatch('init')
                // }, 1000)
            }
            dispatch('list')

        },
        async getList({ commit }) {
            const RegistryOfCapTablesContract = await RegistryOfCapTables.init(ethereum)
            const list = await RegistryOfCapTablesContract.list()
            commit('capTables', { capTables: list })
            return list
        }
    },
    mutations: {
        capTables(state, { capTables }) {
            Vue.set(state, 'capTables', capTables)
        }
    },
    namespaced: true
}