import Vue from "vue";
// Get Browser ethereum client
let ethereum
if (typeof window.ethereum !== 'undefined' || (typeof window.web3 !== 'undefined')) {
    ethereum = window.ethereum
}


const config = {
    desiredNetwork: production ? "53387025" : "53387025",
}
const production = () => {
    return process.env.NODE_ENV === 'production'
}

export default {
    state: {
        initialized: false,
        network: null,
        networkDesired: "6174", //TODO Fix to dynamic config.desiredNetwork
        accounts: [],
        error: null,
        found: false,
        listeners: false,
    },
    getters: {
        currentAccount: state => state.accounts[0],
        notDesiredNetwork: state => (state.network !== state.networkDesired), 
        metamaskNotFound: state => !state.found
    },
    actions: {
        // Called to initiate the model
        async init({ dispatch, commit, state }) {
            // Stop if allreadu initialized
            console.log("metamask.store init");

            // Stop if Metamask not found
            await dispatch('checkMetamask')
            if (!state.found) return false

            // Since we have metamask, we can now add listeners
            dispatch('initMetamaskListeners')

            // Stop if not correct network
            await dispatch('checkNetwork')
            if (state.network !== state.networkDesired) return false

            if (state.accounts.length < 1) {
                await dispatch('connectMetamask')
            }
            commit("initialized", { initialized: true })
            //  dispatch('capTable/init', { provider: ethereum }, { root: true })
        },
        async connectMetamask({ commit }) {
            try {
                const accounts = await ethereum.enable()
                commit('accounts', { accounts })
                return true
            } catch (error) {
                // Handle error. Likely the user rejected the login:
                console.log("Metamask reject connect error: ", error)
                commit('error', { error: error.message })
                return false
            }
        },
        checkMetamask({ commit }) {
            const foundEthereum = (ethereum !== undefined)
            commit('found', { found: foundEthereum })
        },
        checkNetwork({ commit }) {
            commit('network', { network: ethereum.networkVersion })
            if (ethereum.networkVersion !== this.networkDesired) {
                // return as we dont want to connect to metamask if we have the wrong network
                return false;
            }
            return true
        },
        initMetamaskListeners({ dispatch, state, commit }) {
            // Do not add the listeners twice
            if (state.initialized) {
                return false;
            }

            ethereum.on('accountsChanged', function (accounts) {
                console.log("accounts changed => ", accounts)
                commit('accounts', { accounts })
                dispatch('init')
            })
            ethereum.on('networkChanged', function (network) {
                console.log("network changed => ", network)
                dispatch('init')
            })
            commit("listeners", { listeners: true })
        }
    },
    mutations: {
        error(state, { error }) {
            state.error = error
        },
        accounts(state, { accounts }) {
            Vue.set(state, 'accounts', [...accounts]);
        },
        network(state, { network }) {
            state.network = network
        },
        found(state, { found }) {
            state.found = found
        },
        initialized(state, { initialized }) {
            state.initialized = initialized
        },
        listeners(state, { listeners }) {
            state.listeners = listeners
        },
    },
    namespaced: true
};
