const EntityRegistry = require("@brreg/sdk").EntityRegistry

let ethereum
if (typeof window.ethereum !== 'undefined' || (typeof window.web3 !== 'undefined')) {
    ethereum = window.ethereum
}

const EXIST = {
    UNKNOWN: 0,
    TRUE: 1,
    FALSE: 2
}
export default {
    state: {
        exist: EXIST.UNKNOWN,
        address: "",
        uuid: "",
        type: "",
        name: "",
        country: "",
        city: "",
        postalcode: "",
        streetAddress: "",
    },
    actions: {
        async setCurrentEntity({ dispatch, commit }, { address }) {
            try {
                const data = await dispatch('getEntityByAddress', { address })
                commit('address', data.address)
                commit('uuid', data.uuid)
                commit('type', data.type)
                commit('name', data.name)
                commit('country', data.country)
                commit('city', data.city)
                commit('postalcode', data.postalcode)
                commit('streetAddress', data.streetAddress)
                commit('exist', EXIST.TRUE)
                return data
            } catch (error) {
                console.log("Could not find any entity for you so please create one")
                commit('exist', EXIST.FALSE)
            }

        },
        async create({ dispatch }, data) {
            const entityRegistry = await EntityRegistry.init(ethereum)
            const res = await entityRegistry.addEntity({
                address: data.address,
                uuid: data.uuid,
                type: data.type,
                name: data.name,
                country: data.country,
                city: data.city,
                postalcode: data.postalcode,
                streetAddress: data.streetAddress
            })
            await dispatch('setCurrentEntity', { address: data.address })
        },
        async getEntityByUuid({ }, { uuid }) {
            const entityRegistry = await EntityRegistry.init(ethereum)
            const data = await entityRegistry.getEntityByUuid(uuid)
            //console.log("getEntityByUuid data", data)
            return data
        },
        async getEntityByAddress({ }, { address }) {
            const entityRegistry = await EntityRegistry.init(ethereum)
            const data = await entityRegistry.getEntityByAddress(address)
            //console.log("getEntityByAddress data", data)
            return data
        },
        async allTransactionsByCapTable({ }, { uuid }) {
            const entityRegistry = await EntityRegistry.init(ethereum)
            const data = await entityRegistry.allTransactionsAllCapTables(uuid)
            console.log("getTransactions data ", data)
            return data
        },
        async generateAddress({ }) {
            const entityRegistry = await EntityRegistry.init(ethereum)
            const address = await entityRegistry.generateAddress()
            //console.log("getTransactions data ", data)
            return address
        },

    },
    mutations: {
        exist(state, data) {
            state.exist = data
        },
        address(state, data) {
            state.address = data
        },
        uuid(state, data) {
            state.uuid = data
        },
        type(state, data) {
            state.type = data
        },
        name(state, data) {
            state.name = data
        },
        country(state, data) {
            state.country = data
        },
        city(state, data) {
            state.city = data
        },
        postalcode(state, data) {
            state.postalcode = data
        },
        streetAddress(state, data) {
            state.streetAddress = data
        },
    },
    namespaced: true
};
