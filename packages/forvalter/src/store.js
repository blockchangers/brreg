import Vue from 'vue'
import Vuex from 'vuex'

import metamask from './stores/metamask.store';
import registryOfCapTables from './stores/registryOfCapTables.store';
import registryOfCapTablesQue from './stores/registryOfCapTablesQue.store';
import capTable from './stores/capTable.store';
import entity from './stores/entity.store';

Vue.use(Vuex)

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: {
    metamask, registryOfCapTables, registryOfCapTablesQue, capTable, entity
  },
})
