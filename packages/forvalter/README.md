# @Brreg/forvalter 🕵️‍♂️

A frontend to manage Brreg Cat Table Registry. 

## Setup this frontend locally. 🌅

1. git clone git@gitlab.com:blockchangers/brreg.git
2. npm i
3. npx lerna bootstrap
4. npm run build
5. npm run forvalter

## Software used in this project

GIT (https://git-scm.com/)
NODE & NPM (https://nodejs.org/en/download/)
(windows) RUN "npm install --global windows-build-tools" after NPM has been installed( https://www.npmjs.com/package/windows-build-tools )
VUE cli (https://cli.vuejs.org/guide/installation.html)
MetaMask (https://metamask.io/)