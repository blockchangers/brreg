# Brreg Monorepo 

## Packages
* [**SDK**](https://gitlab.com/blockchangers/brreg/tree/master/packages/sdk) ✨- An NPM package that makes it easy to talk with Brreg Cap Tables. 
* [**Contracts**](https://gitlab.com/blockchangers/brreg/tree/master/packages/contracts) 🥣 - Smart-contracts involved and deployment tests.
* [**Forvalter**](https://gitlab.com/blockchangers/brreg/tree/master/packages/forvalter) 🕵️‍♂️ - Frontend to manage this system. Also works as an example implementation.

# Setup Development 🤓
* `npm i -g typescript`
* `npm i`
* `npx lerna bootstrap`
* `npx lerna run compile`
* `npx lerna run build`

## Develop 😎
* `npx lerna run watch --parallel` running in a window to continously build source files for projects.
* `npx run forvalter` to have Forvalter app run on local HTTP server
* Contracts and SDK developing should be run alongside their test in /test folder.

# Deploy
* Forvalter app is automically deployed [here](https://blockchangers.gitlab.io/brreg/). This is a Gitlab CD job running [pipelines](https://gitlab.com/blockchangers/brreg/pipelines). The setup for this is [here](https://gitlab.com/blockchangers/brreg/blob/9d834137ab5f84688261860b37665fc6b65a844b/.gitlab-ci.yml)
* To deploy new versions of SDK and Contracts. use ```npx lerna publish [major, minor, patch]```

## What is monorepo 
It is a software development strategy where code for many projects are stored in the same repository.